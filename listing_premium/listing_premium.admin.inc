<?php // $Id: listing_premium.admin.inc,v 1.0 2011/03/17 09:00:32 newzeal Exp $
function listing_premium_settings_form() {
  $form['listing_premium_node_view_alert'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Node View Alert'),
    '#default_value' => variable_get('listing_premium_node_view_alert', 1),
    '#description'   => t("Display a link for the owner of a listing node on the view page to the premium listings application page if the node is not already upgraded."),
  );

  $form['listing_premium_remove_free'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Remove Free Listings from Cart'),
    '#default_value' => variable_get('listing_premium_remove_free', 0),
    '#description'   => t("Sometimes you might include an option for a free listing.  If so, and you enable this setting, then if a listing item hits the cart with a zero price then it is removed and the user returns to the listing node page."),
  );
  if (module_exists('views')) {
    $views_list = array();
    foreach (views_get_all_views() as $view => $viewdata) {
      $views_list[$viewdata->name] = $viewdata->name;
    }
	asort($views_list);
    $form['listing_premium_views'] = array(
      '#type'          => 'select',
	  '#options'		 => $views_list,
	  '#multiple'		 => TRUE,
      '#title'         => t('Views'),
      '#default_value' => variable_get('listing_premium_views', array()),
      '#description'   => t("Select the views to which you wish to apply listing premium filtering.  When filtering is on, then fields will be displayed for nodes according to the rules created by the different listing premium levels."),
    );
  }
  $options = node_get_types('names');
  $form['listing_premium_published_ctypes'] = array(
    '#type'          => 'select',
	'#options'		 => variable_get('listing_content_types', array()),
	'#multiple' 	 => TRUE,
    '#title'         => t('Content types that remained published.'),
	'#description'   => t("You may wish to have listings that are free to publish but which contain fields that people need to purchase in order to make visible.  It is easy to configure this to happen, you simply don't select the content type to be controlled by the scheduler module, since listing premium keeps its own record. However, if you want to send out email notifications when fields are unpublished then you need to select the relevant ocntent types here."),
    '#default_value' => variable_get('listing_premium_published_ctypes', array())
  );
  $form['#validate'][] = 'listing_premium_settings_validate';
  return system_settings_form($form);
}
function listing_premium_settings_validate($form, $form_state) {
  //if (!is_numeric($form_state['values']['listing_premium_levels'])) form_set_error('listing_premium_levels', t('The number of levels has to be numeric'));
}

function listing_premium_levels_admin() {
  $output = l('Add a Premium listing level', 'admin/listing/premium/levels/add');
  $result = db_query("SELECT * FROM {listing_premium_levels}");
  $rows = array();
  $header = array('Name', 'Description', 'Weight', 'Operation');
  while ($row = db_fetch_object($result)) {
    $rows[$row->lpid]['name'] = $row->name;
    $rows[$row->lpid]['description'] = $row->description;
	$rows[$row->lpid]['weight'] = $row->weight;
    $rows[$row->lpid]['edit'] = l('Edit', 'admin/listing/premium/levels/'.$row->lpid.'/edit');
  }
  $output .= theme('table', $header, $rows);
  return $output;
}
function listing_premium_level_form($form_state, $listing_level=NULL) {
  $form['name'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Name'),
      '#default_value' => isset($listing_level->name) ? $listing_level->name : '',
      '#description'   => t("The name of this premium listing level."),
  );
  $form['description'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Description'),
      '#default_value' => isset($listing_level->description) ? $listing_level->description : '',
      '#description'   => t("The description for this premium listing level."),
  );
  for ($i=0; $i<20; $i++) {
    $options[$i] = $i;
  }
  $form['weight'] = array(
      '#type'          => 'select',
	  '#options' 	   => $options,
      '#title'         => t('Weight'),
      '#default_value' => isset($listing_level->weight) ? $listing_level->weight : '',
      '#description'   => t("The weight for this level. To use for sorting in Views."),
  );
  $types = uc_product_types();
  $placeholders = array();
  foreach ($types as $type) $placeholders[] = "'%s'";
  $options = array();
  $result = db_query("SELECT nid, title FROM {node} WHERE type IN (".implode(",", $placeholders).")", $types);
  while ($row = db_fetch_object($result)) {
	  $options[$row->nid] = $row->title;
  }
  if (empty($options)) {
    $error['product_nid'] = array(
      '#type'          => 'markup',
      '#value'         => t('You need to create a product before you can create a listing level.'),
    );  
	return $error;
  }
  else {
    $form['product_nid'] = array(
      '#type'          => 'select',
	  '#options' 	   => $options,
      '#title'         => t('Product'),
      '#default_value' => isset($listing_level->product_nid) ? $listing_level->product_nid : '',
      '#description'   => t("The product to associate when purchasing this premium listing level."),
    );
  }
  $options = array();
  foreach (content_fields() as $field) {
	$options[$field['field_name']] = $field['widget']['label']. ' (' .$field['type_name']. ')';
  }
  $form['data'] = array(
      '#type'          => 'select',
	  '#options' 	   => $options,
	  '#multiple'	   => TRUE,
      '#title'         => t('Fields'),
      '#default_value' => isset($listing_level->data) ? unserialize($listing_level->data) : array(),
      '#description'   => t("Whichever fields you select here will not display unless the node has been upgraded using this listing level or by another listing level. In other words, select all the fields that you wish to display that do not appear in your free or default listing."),
  );
  $form['duration_units'] = array(
	  '#prefix' => '<div>'. t('The length of time which the upgrade applies for.  Note that this will not be used if a listing is mapped to a product through uc_node_checkout and uses attributes.').'</div>',
	  '#type' => 'textfield',
	  '#size' => 4,
	  '#title' => t('Number of Units'),
	  '#default_value' => isset($listing_level->duration_units) ? $listing_level->duration_units : 1,
	  '#description' => t('A zero value will disable this feature and node will publish immediately on purchase.'),
  );
  $form['duration_period'] = array(
	  '#type' => 'select',
	  '#title' => t('Period'),
	  '#options' => uc_node_checkout_scheduler_duration_options(),
	  '#default_value' => isset($listing_level->duration_period) ? $listing_level->duration_period : '',
  );
  $form['lpid'] = array(
      '#type'          => 'hidden',
      '#value'         => isset($listing_level->lpid) ? $listing_level->lpid : 0,
  );
  $form['save'] = array(
      '#type'          => 'submit',
      '#value'         => t('Save'),
  );
  $form['delete'] = array(
      '#type'          => 'submit',
      '#value'         => t('Delete'),
  );
  return $form;
}
function listing_premium_level_form_submit($form, $form_state) {
  $type = array();
  foreach (content_fields() as $field) {
    
	$type[$field['field_name']] = $field['type'];
  }
  $map = listing_premium_field_map();
  if ($form_state['values']['op'] == t("Save")) {
    $fields = $form_state['values']['data'] ;
	
    $form_state['values']['data'] = serialize($form_state['values']['data']);
    if ($form_state['values']['lpid'] > 0) {
	  drupal_write_record('listing_premium_levels', $form_state['values'], 'lpid');
	  $lpid = $form_state['values']['lpid'];
    }
    else {
	  drupal_write_record('listing_premium_levels', $form_state['values']);
	  $lpid = db_last_insert_id('listing_premium_levels', 'lpid');
    }
    db_query("DELETE FROM {listing_premium_fields} WHERE lpid=%d", $lpid);
    foreach ($fields as $field) {
	  $extension = $map[$type[$field]];
	  if (!$extension) $extension = 'value';
      db_query("INSERT INTO {listing_premium_fields} (field_name, ext, lpid, active) VALUES ('%s', '%s', %d, 1)", $field, $extension, $lpid);
	  $count = db_result(db_query("SELECT COUNT(*) FROM {listing_premium_fields}"));
    }
  }
  // We need to have a confirm form here because if we delete a level then this mucks up any nodes already associated with it
  elseif ($form_state['values']['op'] == t("Delete")) {
    if ($form_state['values']['lpid'] > 0) {
	  //db_query("DELETE FROM {listing_premium_fields} WHERE lpid=%d", $form_state['values']['lpid']);
	  //db_query("DELETE FROM {listing_premium_levels} WHERE lpid=%d", $form_state['values']['lpid']);
	}
  }
  drupal_goto('admin/listing/premium/levels');
}
function listing_premium_info_form($form_state, $node) {
  if (!isset($node->listing_premium)) {
    $info = (array) listing_premium_listing_node_info_all($node->nid);
    $node->listing_premium = $info;
  }
  $product_nid = db_result(db_query("SELECT product_nid FROM {uc_node_checkout_types} WHERE node_type='%s'", $node->type));
  $result = db_query("SELECT lpid, name FROM {listing_premium_levels} WHERE product_nid=%d", $product_nid);
  $options = array();
  while ($row = db_fetch_object($result)) {
    $options[$row->lpid] = $row->name;
  }
  $form['eplanation'] = array(
      '#type'          => 'markup',
	  '#value' 	   	   => t('This form is to be used for remedial work only when things go wrong and is not meant for general use.'),
  );
  $form['lpid'] = array(
      '#type'          => 'select',
	  '#options' 	   => $options,
      '#title'         => t('Listing Level'),
      '#default_value' => $node->listing_premium['lpid'],
      '#description'   => t("The listing level."),
  );
  $result = db_query("SELECT po.oid, ao.name FROM {uc_product_options} po LEFT JOIN {uc_attribute_options} ao ON po.oid=ao.oid WHERE po.nid=%d", $product_nid);
  $options = array();
  while ($row = db_fetch_object($result)) {
    $options[$row->oid] = $row->name;
  }
  $form['oid'] = array(
      '#type'          => 'select',
	  '#options' 	   => $options,
      '#title'         => t('Ubercart Attribute Option'),
      '#default_value' => $node->listing_premium['lpid'],
      '#description'   => t("The listing level."),
  );
  $form['created'] = array(
	  '#type' => 'textfield',
	  '#title' => t('Created'),
	  '#description' => t('The create date in the form yyyy-mm-ddThh:mm:ss where \'T\' is a divider'),
	  '#default_value' => date('Y-m-d\TH:i:s', $node->listing_premium['created']),
  );
  $form['expiry'] = array(
	  '#type' => 'textfield',
	  '#title' => t('Expiry'),
	  '#description' => t('The expiry date in the form yyyy-mm-ddThh:mm:ss where \'T\' is a divider'),
	  '#default_value' => date('Y-m-d\TH:i:s', $node->listing_premium['expiry']),
  );
  $form['status'] = array(
	  '#type' => 'select',
	  '#options' => array(0, 1),
	  '#title' => t('Status'),
	  '#description' => t('The status of the node.  This is usually 0 when the node has expired, or 1 when it is current'),
	  '#default_value' => $node->listing_premium['status'],
  );
  $form['id'] = array(
    '#type' => 'hidden',
	'#value' => $node->nid .'-'. $node->listing_premium['created']
  );
  $form['submit'] = array(
    '#type' => 'submit',
	'#value' => t('Submit')
  );
  return $form;
}
function listing_premium_info_form_submit($form, $form_state) {
  $form_state['values']['created'] = strtotime($form_state['values']['created']); 
  $form_state['values']['expiry'] = strtotime($form_state['values']['expiry']); 
  $parts = explode("-", $form_state['values']['id']);
  db_query("UPDATE {listing_premium_nodes} SET lpid=%d, oid=%d, created=%d, expiry=%d, status=%d WHERE nid=%d AND created=%d", $form_state['values']['lpid'], $form_state['values']['oid'], $form_state['values']['created'], $form_state['values']['expiry'], $form_state['values']['status'], $parts[0], $parts[1]);
}
/*
 *  Helper function to map the field type name to the extension that cck gives the field name in the database.  This is so that the field name can be detected in views aliases
 */
function listing_premium_field_map() {
  return array(
    'text' => 'value',
	'nodereference' => 'nid',
	'filefield' => 'fid',
  );
}