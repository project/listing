24 Sept 2011
Listing_premium was republishing nodes for some reason after scheduler had unpublished them.  Changed this.

6 Jan 2012
.info file incorrectly showed uc_attribute module as uc_attributes
Fixed listing preview so it works correctly
Add default uc_attributes value to node form