<?php // $Id: listing_premium.module,v 1.0 2011/03/17 09:00:32 newzeal Exp $

require_once 'listing_premium.ca.inc';

/**
 * Implementation of hook_help).
 */

function listing_premium_help($path, $arg) {
  switch ($path) {
    case 'admin/help#listing_premium':
      return '<h3>Listing Premium Module</h3>
		<p>This is an add-on to the listing module which interfaces with Ubercart to enable listings to be upgraded via uc_node_checkout.</p>
		<p>Node checkout is normally used to redirect to a purchase on node creation but in the listing system we allow listing nodes to be created for free.  Listing premium simply enables listings to be upgraded using the same uc_node_checkout functionality and the listing_premium settings are used instead of the uc_node_checkout settings to create the association, so unless you wish to make some nodes publishable only on payment then do not use the uc_node_checkout settings.</p>
        ';
  }
}
/**
 * Implementation of hook_menu().
 */

function listing_premium_menu() {
  $items['admin/listing/premium'] = array(
    'title' => 'Premium Listings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('listing_premium_settings_form'),
    'access arguments' => array('administer premium listings'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'listing_premium.admin.inc',
    'weight' => 4,	  
  );
  $items['admin/listing/premium/settings'] = array(
    'title' => 'Premium Listings',
    'access arguments' => array('administer premium listings'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 0,	  
  );
  $items['admin/listing/premium/levels'] = array(
    'title' => 'Premium Listing Levels',
    'page callback' => 'listing_premium_levels_admin',
    'access arguments' => array('administer premium listings'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'listing_premium.admin.inc',
    'weight' => 4,	  
  );
  $items['admin/listing/premium/levels/add'] = array(
    'title' => 'Add a Premium Listing Level',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('listing_premium_level_form'),
    'access arguments' => array('administer premium listings'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'listing_premium.admin.inc',
    'weight' => 4,	  
  );
  $items['admin/listing/premium/levels/%listing_premium_level/edit'] = array(
    'title' => 'Edit a Premium Listing Level',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('listing_premium_level_form', 4),
    'access arguments' => array('administer premium listings'),
    'file' => 'listing_premium.admin.inc',
    'weight' => 4,	  
  );
  $items['node/%node/listing/preview'] = array(
    'title' => 'Preview',
    'page callback' => 'listing_premium_preview',
    'page arguments' => array(1),
    'access callback' => 'listing_access',
    'access arguments' => array(1),
    'weight' => 1,
    'file' => 'listing_premium.pages.inc',
    'type' => MENU_LOCAL_TASK,
  );
  $items['node/%node/listing/edit'] = array(
    'title' => 'Listing Info',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('listing_premium_info_form', 1),
    'access callback' => 'listing_premium_info_access',
    'access arguments' => array(1),
    'weight' => 1,
    'file' => 'listing_premium.admin.inc',
    'type' => MENU_LOCAL_TASK,
  );
  return $items;
}
function listing_premium_info_access($node) {
  if (!user_access('administer premium listings')) return FALSE;
  //  Only return true if this listing is current or there is a record to be edited
  $info = listing_premium_listing_node_info_all($node->nid);
  if (!empty($info)) return TRUE;
}
/**
 * Implementation of hook_perm).
 */
function listing_premium_perm() {
  return array('administer premium listings');
}
/**
 * Implementation of hook_form_alter().
 */
function listing_premium_form_alter(&$form, &$form_state, $form_id) {
  global $user;
  
  // Alters the product add to cart forms to redirect to the appropriate node.  This is only for the node/%node/listing page
  // add form if enabled.
  if ((strpos($form_id, 'add_to_cart_form') > 0 || strpos($form_id, 'uc_catalog_buy_it_now_form_') === 0) && arg(0)=="node" && is_numeric(arg(1))) {
    // If a node type association exists for this product...
	
    if ($form['nid']['#value'] > 0) {
      // Store the node type in the form.
      $form['product_nid'] = array(
        '#type' => 'hidden',
        '#value' => $form['nid']['#value'],
      );
      $form['node_checkout_nid'] = array(
        '#type' => 'hidden',
        '#value' => arg(1),
      );

      // Add the submit handler to use the add to cart redirect handler.
      //$form['submit']['#submit'][] = 'listing_premium_add_to_cart_submit';
    }
  }
  if ($form_id == "uc_node_checkout_scheduler_admin_settings") {
    if (isset($form['start_units_0_0_'.arg(3)])) {
	  $form['listing_premium_explanation'] = array(
	    '#type' => 'markup',
		'#value' => '<b>'.t('There is no product mapped to this content type using uc_node_checkout, and the listing_premimum module is using this content type, therefore these settings will not apply. Instead the settings (!settings) for the premium listing type will apply.', array('!settings' => 'admin/listing/premium/levels')).'</b>',
		'#weight' => -20
	  );
	}
  }
  
  if (in_array($form['#node']->type, variable_get('listing_content_types', array()))) {
    //  Add validation for fields based on the listing level selected and the fields used for that listing level
    $form['#validate'][] = 'listing_premium_validate_listing';
	//  To ensure the presence of default values into the attributes form we need to tell uc_node_checkout what they are
	//  For a node in the listing_premium system we use those defaults, otherwise we use the product attribute defaults
	$info = array();
    if ($form['nid']['#value'] > 0) {
      $info = listing_premium_listing_node_info($form['nid']['#value']);
	  $aid = db_result(db_query("SELECT aid FROM uc_attribute_options WHERE oid=%d", $info->oid));
	  $_GET['attr'] = $aid.'-'.$info->oid;
    }
	if (empty($info)) { 
      // Alter the node forms for UC Node Checkout governed node types by ensuring a default value as per uc_attribute settings
      foreach (uc_node_checkout_product_map() as $type => $value) {
        if ($form_id == $type .'_node_form') {
          if (module_exists('uc_attribute') && empty($form['#node']->uc_order_product_id)) {
            // Load up any attribute elements for this node's form.
 		    $attributes = uc_product_get_attributes($value['nid']);
            foreach ( $attributes as $aid => $values) {
              $attr[] = $aid .'-'. $values->default_option;
            }

            if (!empty($attr)) {
              $_GET['attr'] = implode('_', $attr);
			}
		  }
        }
      }		
	}
  }
}
function listing_premium_validate_listing($form, $form_state) {
  
}
/**
 * Implementation of hook_theme().
 */
function listing_premium_theme() {
  return array(
    'listing_premium_node_info' => array(
      'arguments' => array('info' => NULL, 'status' => NULL),
    ),
    'listing_premium_preview' => array(
      'arguments' => array('lpid' => NULL, 'level' => NULL, 'view' => NULL),
	  'template' => 'listing-premium-preview',
	  'path' => drupal_get_path('module', 'listing_premium').'/templates'
    ),
    'listing_premium_inactive_message' => array(
      'arguments' => array('node' => NULL),
	  'template' => 'listing-premium-inactive-message',
	  'path' => drupal_get_path('module', 'listing_premium').'/templates'
    ),
  );
}

/**
 * Implementation of hook_nodeapi().
 */
function listing_premium_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  global $user;
  switch($op) {
    //case "validate":
	case 'load':
	  $info = listing_premium_listing_node_info($node->nid);
	  if ($info) $node->listing_premium = (array) $info;
	  break;
	case "view":
	  // Filter out fields that do not fit the listing criteria
	  //if ($user->uid != $node->uid && $user->uid != 1) {
	  if (in_array($node->type, variable_get('listing_content_types', array()))) {
	    $n = (array) $node;
		if ($node->listing_premium['lpid'] > 0) $hide_all = FALSE;
		else {
		  //  This ensures that listings not in the listing premium system are treated as non premium nodes
		  $hide_all = TRUE;	  
		  $node->content['listing-premium'] = array(
		    '#value' => theme('listing_premium_inactive_message', $node),
		  );
		}
		
        $fields = listing_premium_get_fields();
	    foreach ($n as $key => $field) {
	      if (substr($key, 0, 5) == "field") {
		    if (isset($fields[$key])) {
			  $display = FALSE;
			  foreach ($fields[$key] as $value) {
			    // For this field to be viewable there should be a record in the listing_premium_nodes table.  We assume only one listing is present
				// TODO add expiry and or status info 
				if (!$hide_all && $value['lpid'] == $node->listing_premium['lpid']) {
				  $display = TRUE;
				  
				}
			  }
			  //  No valid display record has been found
			  if (!$display) {
			    unset($node->content[$key]);
			  }
			}
		  }
	    }
	  }
	  
	  break;
    case "insert":
	  //fall through
	case "update":
	  break;
    case "delete":
	  db_query("DELETE FROM {listing_premium_nodes} WHERE nid=%d", $node->nid);
   }
}
function listing_premium_filter_node_view($n, &$node, $lpid) {

}
/**
 * Themes the information available for a product when the node was purchased on
 *   the node view page.
 *
 * @param $info
 *   The info array for the node from listing_premium if available
 * @param $status
 *   Whether or not the node is published
 * @return
 *   An HTML string representing the attribute selections from the purchase of
 *     the node.
 */
function theme_listing_premium_node_info($info, $status) {
  if (!empty($info)) {
	$level = db_result(db_query("SELECT name FROM {listing_premium_levels} WHERE lpid=%d", $info->lpid));
	$option = db_result(db_query("SELECT name FROM {uc_attribute_options} WHERE oid=%d", $info->oid));
	$output = t('This is a !level listing of !type and will expire on !date', array('!level' => $level, '!type' => $option, '!date' => format_date($info->expiry)));
  }
  // If something goes wrong then it is possible that the node is published while not having any premium listing info
  else {
    if ($status) $output = t('This node is published');
    else $output = t('This node is unpublished');
  }
  $output = '<div class="ucncs-info"><div class="ucncs-attributes-label">'. t('Information about this listing:')
           .'</div>'. $output .'</div>';

  return $output;
}

/**
 * Implementation of hook_views_api().
 */
function listing_premium_views_api() {
  return array(
    'api' => 2,
    'path' => drupal_get_path('module', 'listing_premium') .'/views',
  );
}
/**
 * Implementation of hook_cron().
 */
function listing_premium_cron() {
  // The scheduler module updates nodes on the cron run and so this cron process will synchronize with that.  
  //  In addition, if a listing is renewed and takes on different features then we may need to be able to do other things to the listing, hence the hook function
  //  We look at all active listings which have expired and those which fit into the current time span and whose status is 0
  // TODO Do we need some timezone sensitive stuff here?
  //$result = db_query("SELECT * FROM {listing_premium_nodes} WHERE status=1 AND expiry<%d", time());
  // We get the node type so we can test if it is a field only premium listing
  $result = db_query("SELECT l.*, n.type FROM {listing_premium_nodes} l LEFT JOIN {node} n ON l.nid=n.nid WHERE l.status=1 AND l.expiry<%d", time());
  $expired = array();
  while ($row = db_fetch_object($result)) {
    // delete any that have expired
	$result2 = db_query("SELECT * FROM {listing_premium_nodes} WHERE nid=%d AND lpid=%d AND created=%d", $row->nid, $row->lpid, $row->created);
	$items = array();
	while ($row2 = db_fetch_object($result2)) {
	  $items[] = $row2;
	}
	//watchdog('listing_premium', 'cron items to delete '.print_r($items,1));
	db_query("UPDATE {listing_premium_nodes} SET status=0 WHERE nid=%d AND lpid=%d AND created=%d", $row->nid, $row->lpid, $row->created);
    //db_query("DELETE FROM {listing_premium_nodes} WHERE nid=%d AND lpid=%d AND created=%d", $row->nid, $row->lpid, $row->created);
	$row->status = 0;
	$expired[$row->nid] = $row;
  }
  $created = array();
  // We get the node type so we can test if it is a field only premium listing
  $result = db_query("SELECT l.*, n.type FROM {listing_premium_nodes} l LEFT JOIN {node} n ON l.nid=n.nid WHERE l.status=0 AND l.created<=%d AND l.expiry>=%d", time(), time());
  while ($row = db_fetch_object($result)) {
	// set status to 1 for any that are now current
    db_query("UPDATE {listing_premium_nodes} SET status=1 WHERE nid=%d AND lpid=%d AND created=%d", $row->nid, $row->lpid, $row->created);
	$row->status = 1;
	$created[$row->nid] = $row;
  }
  // Allow modules to manipulate nodes whose status has been modified
  foreach(module_implements('listing_premium_update_status') as $module) {
    $func = $module .'_listing_premium_update_status';
	$func($expired, $created);
  }
  
  // Check for any nodes which are inactive in listing_premium and are active for scheduler
  $types = node_get_types();
  $ctypes = array();
  foreach ($types as $name => $types) {
    if (variable_get('scheduler_unpublish_enable_'. $name, 0)) $ctypes[] = "'".$name."'";
  }
  $result = db_query("SELECT pn.* FROM {listing_premium_nodes} pn LEFT JOIN {node} n ON n.nid=pn.nid WHERE pn.status=0 AND n.status=1 AND n.type IN (". implode(',', $ctypes ) .") ", time());
  while ($row = db_fetch_object($result)) {
    //watchdog('listing_premium', 'expired but still published '.print_r($row,1));
  }
  /*
     *  Content types which remain published but have fields that can be unpublished are dealt with here
     */
  $fields_types = variable_get('listing_premium_published_ctypes', array());
  if (!empty($fields_types)) {
    foreach ($expired as $nid => $row) {
      // We process it if the node is not being renewed straight away and is one of our selected content types which remain published
      if (in_array($row->type, $fields_types) && !isset($created[$nid])) {
        // Now we check if the node has been upgraded already
	    $count = db_result(db_query("SELECT COUNT(*) FROM {listing_premium_nodes} WHERE created<=%d AND expiry>=%d", time(), time()));
	    if (!$count) {
	      // Still not active, then lets trigger an alert
		
		  $node = node_load($row->nid);
		  $account = user_load($node->uid);
          if (ca_pull_trigger('listing_premium_notify_fields', $account, $node)) {}
		  else watchdog('listing_premium', '@type: reminder of impending fields unpublish of %title failed.', array('@type' => $node->type, '%title' => $node->title), WATCHDOG_NOTICE, l(t('view'), 'node/'. $node->nid));
		
	    }
      }	
	}
  }
}

/**
 * Implementation of hook_views_pre_render().
 */
function listing_premium_views_pre_render(&$view) {
  // Apply premium listing filter to relevant fields
  
  if (in_array($view->name, variable_get('listing_premium_views', array()))) {
	// First grab all the nids
	$nids = array();
	foreach ($view->result as $node) {
	  $nids[] = $node->nid;
	}
	if (empty($nids)) return;
	// Now get all listing_premium data for these nodes and put into a sensible array
	$result = db_query("SELECT lpid, nid FROM {listing_premium_nodes} WHERE nid IN (". implode(',', $nids ) .") AND created < %d AND expiry > %d", time(), time());
	$levels = array();
	while ($row = db_fetch_object($result)) {
	  $levels[$row->nid] = $row->lpid;
	}
	// Get the field data and go through and suppress fields where applicable
	$fields = listing_premium_get_fields_by_level();
	foreach ($view->result as $key => $node) {
	  //  If there is no listing level then we will hide all premium fields.  This is to cater for sites that already have listings when this module is enabled
	  
	  if (isset($levels[$node->nid])) $hide_all = FALSE;
	  else $hide_all = TRUE;
	  if (is_array($fields[$levels[$node->nid]]) || $hide_all) {
		$n = (array) $node;
		foreach ($n as $f => $val) {
		  $display = FALSE;
		  
		  // we only process cck fields.  These all have the format [field-name]_value in their name.  Views creates an alias so we cannot do a straight comparison
		  if (strpos($f, 'field')) {
		    if (!$hide_all) {
		      foreach ($fields[$levels[$node->nid]] as $field => $values) {
		        if (strpos($f, $field.'_'.$values->extension)) {
			      $display = TRUE;
			    }
		      }
			}
		    if (!$display || $hide_all) {
			  unset($view->result[$key]->{$f});
			}
		  }
		}
	  }
	}
  }
}
function listing_premium_add_to_cart_submit($form, $form_state) {
}
/**
 * Implementation of hook_add_to_cart_data().
 */
function listing_premium_add_to_cart_data($form_values) {
  // We add the necessary info to the data array for uc_node_checkout
  return array('nid' => $form_values['product_nid'], 'node_checkout_nid' => $form_values['node_checkout_nid']);
}
/**
 * Implementation of hook_cart_item().
 */
 
function listing_premium_cart_item($op, &$item) {
  //  If we allow zero priced listings to be removed from the cart then that happens here
  switch ($op) {
    case 'load':
      if (variable_get('listing_premium_remove_free', 0)) {
    
	    $display_item = module_invoke($item->module, 'cart_display', $item);
	    //drupal_set_message('total '.uc_price($display_item['#total'], $context));
        if ($display_item['#total'] == 0 && $item->data['node_checkout_nid'] > 0 && module_exists('uc_attribute')) {
		  //  We could make the weight of listing_premium greater than that of uc_attribute but we will do this instead, in order to avoid the hassle
		  
          $options = _uc_cart_product_get_options($item);

          $price = 0;

          foreach ($options as $option) {
            $price += $option['price'];
          }
		  if ($price == 0) {
	        uc_cart_remove_item($item->nid, $item->cart_id, $item->data);
	        drupal_goto('node/'. $item->data['node_checkout_nid']);
		  }
	    }
      }
  }
}
/**
 * Implementation of hook_scheduler_api().
 */
function listing_premium_scheduler_api($node, $action) {
  //  Scheduler will have unpublished the node but we only want to unpublish some fields so we republish again
  //  TODO what if some nodes are to be unpublished and some to have only fields unpublished
  if ($action == "unpublish") {
  /*
    $actions = array('node_publish_action', 'node_save_action');
    $context['node'] = $node;
    actions_do($actions, $node, $context, NULL, NULL);
	*/
    // Now we do the action as per listing_premium settings
	// TODO what if a node has more than one listing activated
	//db_query("DELETE FROM {listing_premium_nodes} WHERE nid=%d", $node->nid);
  }
}
/*
 *  Implementation of hook_listing_author_info
 */
function listing_premium_listing_author_info($node) {
  // Get the current listing info and the full listing info
  $result = db_query("SELECT pn.*, pn.status, t.name, t.description, o.oid, o.name AS opt FROM {node} n RIGHT JOIN {listing_premium_nodes} pn ON n.nid=pn.nid LEFT JOIN {listing_premium_levels} t ON pn.lpid=t.lpid LEFT JOIN {uc_attribute_options} o ON pn.oid=o.oid WHERE n.nid=%d ORDER BY pn.created DESC", arg(1));
  $i = 0;
  $header = array(t('Name'), t('Description'), t('Option'), t('Start'), t('Expiry'), '');
  $rows = array();
  while ($row = db_fetch_object($result)) {
    $rows[$i]['name'] = $row->name;
	$rows[$i]['description'] = $row->description;
    $rows[$i]['option'] = $row->opt;
    $rows[$i]['created'] = date('j M, Y', $row->created);
    $rows[$i]['expiry'] = date('j M, Y', $row->expiry);
	if ($row->status) $rows[$i]['current'] = t('current');
    $i++;
	if ($row->lpid == $info->lpid) $level = $row->name;
	if ($row->oid == $info->oid) $option = $row->opt;
  }
  if ($info) {
    $output = t('This is a !level listing of !type which will expire on !date', array('!level' => $level, '!type' => $option, '!date' => date('j M, Y', $info->expiry)));
  }
  if ($i>0) $output .= theme('table', $header, $rows);
  // Now we display the options for upgrading this listing
  $output .= '<div id="premium-renew-link">'. l(t('Renew'), 'node/'. arg(1) .'/edit', array('query' => array('type' => 'renew'))) .'</div>';
  return $output;
}

/*
 *  Helper functions
 */
function listing_premium_level_load($lpid) {
  return db_fetch_object(db_query("SELECT * FROM {listing_premium_levels} WHERE lpid=%d", $lpid));
}
function listing_premium_get_levels() {
  $result = db_query("SELECT * FROM {listing_premium_levels}");
  $levels = array();
  while ($row = db_fetch_object($result)) {
    $levels[$row->lpid] = $row;
  }
  return $levels;
}
function listing_premium_get_fields() {
  static $fields;
  if (!isset($fields)) {
    $result = db_query("SELECT * FROM {listing_premium_fields}");
    $fields = array();
    while ($row = db_fetch_object($result)) {
      $fields[$row->field_name][$row->lpid] = $row->lpid;
    }
  }
  return $fields;
}
function listing_premium_get_fields_by_level() {
  static $fields;
  if (!isset($fields)) {
    $result = db_query("SELECT * FROM {listing_premium_fields}");
    $fields = array();
    while ($row = db_fetch_object($result)) {
      $fields[$row->lpid][$row->field_name] = $row;
	  
    }
  }
  return $fields;
}
/*
 *  Function to get the premium listing information about a node
 *  This only serves up listing information that is current; and has not expired yet.
 */
function listing_premium_listing_node_info($nid) {
  static $info;
  if (empty($info[$nid])) {
    $info[$nid] = db_fetch_object(db_query("SELECT * FROM {listing_premium_nodes} WHERE nid=%d AND status=1", $nid));
  }
  return $info[$nid];
}
/*
 *  Function to get the premium listing information about a node
 *  This serves up the most recent record regardless of status.  Used for admin operations to rescue a user listing
 */
function listing_premium_listing_node_info_all($nid) {
  static $info;
  if (empty($info[$nid])) {
    $info[$nid] = db_fetch_object(db_query("SELECT * FROM {listing_premium_nodes} WHERE nid=%d ORDER BY created DESC LIMIT 1", $nid));
  }
  return $info[$nid];
}
/**
 * Implementation of hook_field_access().
 *  We remove fields not paid for
 * @see content_access().
 */
function listing_premium_field_access($op, $field, $account, $node = NULL) {
  global $user;
  if ($node->uid > 0 && $node->uid != $user->uid && $user->uid != 1) {
    switch ($op) {
      case 'view':
	    //  If this is a preview then we skip the filter
	    if ($node->listing_premium['mode'] == 'preview') return TRUE;
	    if ($node &&  in_array($node->type, variable_get('listing_content_types', array()))) {
		  $info = listing_premium_listing_node_info($node->nid);
	      $fields = listing_premium_get_fields();
		  if (isset($fields[$field['field_name']])) {
		    $display = FALSE;
		    foreach ($fields[$field['field_name']] as $value) {
			    // For this field to be viewable there should be a record in the listing_premium_nodes table.  We assume only one listing is present
				// TODO add expiry and or status info 
				if ($value['lpid'] == $info->lpid) $display = TRUE;
		    }
		    //  No valid display record has been found
			if ($display) return TRUE;
		    else {
		      return FALSE;
		    }
		  }
		}
	    break;
    }
  }
  return TRUE;
}