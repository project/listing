<?php // $Id: listing_premium.ca.inc,v 1.2 2010/10/16 08:47:14 newzeal Exp $
/**
 * @file
 * This file contains the Conditional Actions hooks and functions necessary to make the
 * nodes-related entity, conditions, events, and actions work.
 */


/******************************************************************************
 * Conditional Actions Hooks                                                  *
 ******************************************************************************/
/**
 * Implementation of hook_ca_predicate().
 */
function listing_premium_ca_predicate() {
  // Set the order status to "Payment Received" when a payment is received
  // and the balance is less than or equal to 0.
  $configurations['listing_premium_payment_received'] = array(
    '#title' => t('Upgrade listing on full payment'),
    '#description' => t('Only happens when a payment is entered and the balance is <= $0.00.'),
    '#class' => 'payment',
    '#trigger' => 'uc_payment_entered',
    '#status' => 1,
    '#conditions' => array(
      '#operator' => 'AND',
      '#conditions' => array(
        array(
          '#name' => 'uc_payment_condition_order_balance',
          '#title' => t('If the balance is less than or equal to $0.00.'),
          '#argument_map' => array(
            'order' => 'order',
          ),
          '#settings' => array(
            'negate' => FALSE,
            'balance_comparison' => 'less_equal',
          ),
        ),
        array(
          '#name' => 'uc_order_status_condition',
          '#title' => t('If the order status is not already Payment Received.'),
          '#argument_map' => array(
            'order' => 'order',
          ),
          '#settings' => array(
            'negate' => TRUE,
            'order_status' => 'payment_received',
          ),
        ),
      ),
    ),
    '#actions' => array(
      array(
        '#name' => 'listing_premium_upgrade_node',
        '#title' => t('Upgrade the listing.'),
        '#argument_map' => array(
          'order' => 'order',
        ),
        '#settings' => array(
          'order_status' => 'payment_received',
        ),
      ),
    ),
  );
  $args = array(
    'account' => 'account',
    'node' => 'node',
  );


  // Notify the user when a node is unpublished.
  $configurations['listing_premium_notify_fields'] = array(
    '#title' => t('Notify that fields are unpublished'),
    '#description' => t('Notify the customer when they have had fields unpublished.'),
    '#class' => 'notification',
    '#trigger' => 'listing_premium_notify_fields',
    '#status' => 1,
    '#actions' => array(
      array(
        '#name' => 'listing_premium_fields_email',
        '#title' => t('Send an e-mail to the customer'),
        '#argument_map' => $args,
        '#settings' => array(
          'from' => uc_store_email_from(),
          'addresses' => '[mail]',
          'subject' => uc_get_message('listing_premium_unpublish_subject'),
          'message' => uc_get_message('listing_premium_unpublish_message'),
          'format' => 1,
        ),
      ),
    ),
  );
  return $configurations;
}

/**
 * Implementation of hook_ca_action().
 */
function listing_premium_ca_action() {
  $node_arg = array(
    '#entity' => 'node',
    '#title' => t('Node'),
  );
  $order_arg = array(
    '#entity' => 'order',
    '#title' => t('Order'),
  );
  $user_arg = array(
    '#entity' => 'user',
    '#title' => t('User'),
  );
  $actions['listing_premium_upgrade_node'] = array(
    '#title' => t('Upgrade a listing'),
    '#category' => t('Listing'),
    '#callback' => 'listing_premium_upgrade_node',
    '#arguments' => array(
      'order' => $order_arg,
    ),
  );

  // Send an email to a user with a node unpublish
  $actions['listing_premium_fields_email'] = array(
    '#title' => t('Send an email notifying of fields unpublished.'),
    '#category' => t('Node Checkout'),
    '#callback' => 'listing_premium_action_fields_email',
    '#arguments' => array(
      'account' => $user_arg,
      'node' => $node_arg,
    ),
  );

  return $actions;
}
/**
 * Implementation of hook_ca_trigger().
 */
function listing_premium_ca_trigger() {

  $node = array(
    '#entity' => 'node',
    '#title' => t('Node'),
  );
  $account = array(
    '#entity' => 'user',
    '#title' => t('User'),
  );

  $triggers['listing_premium_notify_fields'] = array(
    '#title' => t('Fields have been unpublished'),
    '#category' => t('Node Checkout'),
    '#arguments' => array(
      'user' => $account,
      'node' => $node,
    ),
  );

  return $triggers;
}


function listing_premium_upgrade_node($order) {
  $levels = listing_premium_get_levels();
  $nids = array();
  foreach ($levels as $level) {
    $nids[$level->product_nid] = $level->product_nid;
  }
  $time = time();
  // the function _uc_node_checkout_scheduler_get_future_date() may produce a start time that is after this time so we add some units to it
  //$time += 60*60;
  foreach ($order->products as $product) {
    if (in_array($product->nid, $nids)) {
	  //  The following mirrors uc_node_checkout_scheduler.ca and is repeated in case this predicate comes before the other
	  $node = node_load($product->data['node_checkout_nid']);	
	  // If the listing is currently active then we must renew
	  $info = listing_premium_listing_node_info($node->nid);
	  if($info->expiry > $time) $renew = TRUE;
	  $found = FALSE;
	  //  If attributes are not present then check for default values
	  if (is_array($product->data['attribute_ids'])) {
	    foreach($product->data['attribute_ids'] as $attribute_id => $option_id) {
	      if($settings = uc_node_checkout_scheduler_get_settings($attribute_id, $option_id, $node->type)) {
	        if($settings['start_units']) {
		      if($renew) $start = _uc_node_checkout_scheduler_get_future_date($settings['start_units'], $settings['start_period'], $info->expiry);
	          else $start = _uc_node_checkout_scheduler_get_future_date($settings['start_units'], $settings['start_period']);
	        }
	        else {
		      if($renew) $start = $info->expiry;
		      else {
		        $start = $time;
		        
		      }
		    }
	        if($settings['expire_units']) {
	          $expire = _uc_node_checkout_scheduler_get_future_date($settings['expire_units'], $settings['expire_period'], $start);
	        }
	        else $expire = '';
			$found = TRUE;
	      }
	    }
		
		// We apply the status if applicable.  This is to indicate elsewhere in the module that this is the current listing level
		if ($start <= $time) $status = 1;
		else $status = 0;
		// We can have more than one record per node to ensure that a listing retains its correct listing level until it expires, but just in case we remove any records with the same start date that already exist
	    db_query("DELETE FROM {listing_premium_nodes} WHERE lpid=%d AND nid=%d AND created=%d", $settings['listing_premium_level'], $product->data['node_checkout_nid'], $start);
        db_query("INSERT INTO {listing_premium_nodes} (lpid, nid, oid, created, expiry, status) VALUES (%d, %d, %d, %d, %d, %d)",  $settings['listing_premium_level'], $product->data['node_checkout_nid'], $option_id, $start, $expire, $status);
	  }
	  // If we are not using product attributes for setting the duration then we use the settings in the listing_premium_level
	  elseif (!$found && isset($nids[$product->nid])) {
		if($renew) $start = $info->expiry;
	    else $start = $time;
	    $expire = _uc_node_checkout_scheduler_get_future_date($nids[$product->nid]->duration_units, $nids[$product->nid]->duration_period, $start);
		if ($start <= $time) $status = 1;
		else $status = 0;
	    // only delete if we have a record already in place
	    db_query("DELETE FROM {listing_premium_nodes} WHERE lpid=%d AND nid=%d AND created=%d", $settings['listing_premium_level'], $product->data['node_checkout_nid'], $start);
        db_query("INSERT INTO {listing_premium_nodes} (lpid, nid, oid, created, expiry, status) VALUES (%d, %d, %d, %d, %d, %d)",  $settings['listing_premium_level'], $product->data['node_checkout_nid'], $option_id, $start, $expire, $status);
	    
	  }
	  else {
	    // Case of fail
		watchdog('listing_premium', t('Premium listing fail for order !order by user !user on listing !listing', array('!order' => $order->order_id, '!user' => $order->uid, '!listing' => $product->data['node_checkout_nid'])));
		uc_order_comment_save($order->order_id, $order->uid, t('Your listing has failed to properly process.  Please contact site admin to fix this problem'), 'order');
		return;
	  }
	  //  Allow other modules to do stuff
	  foreach (module_implements('listing_premium_upgrade_node') as $module) {
	    $func = $module .'_listing_premium_upgrade_node';
		$func($node, $product->data);
	  }
	}
  }
}




/**
 * Send an email with order and node replacement tokens.
 *
 * The recipients, subject, and message fields take order token replacements.
 */
function listing_premium_action_fields_email($account, $node, $settings) {
  $language = language_default();

  // Token replacements for the subject and body
  $settings['replacements'] = array(
    'global' => NULL,
    'node' => $node,
    'user' => $account,
  );

  // Replace tokens and parse recipients.
  $recipients = array();
  $addresses = token_replace_multiple($settings['addresses'], $settings['replacements']);
  foreach (explode("\n", $addresses) as $address) {
    $recipients[] = trim($address);
  }
  // Send to each recipient.
  foreach ($recipients as $email) {
    $sent = drupal_mail('uc_order', 'action-mail', $email, $language, $settings, $settings['from']);

    if (!$sent['result']) {
      watchdog('ca', 'Attempt to e-mail @email concerning order @order_id failed.', array('@email' => $email, '@order_id' => $order->order_id), WATCHDOG_ERROR);
    }
  }
}

// Email settings form
function listing_premium_action_fields_email_form($form_state, $settings = array()) {
  return ca_build_email_form($form_state, $settings, array('global', 'user', 'node'));
}