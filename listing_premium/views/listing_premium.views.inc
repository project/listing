<?php
/**
 * Implementation of hook_views_data():
 * Present fields and filters for user data.
 */
function listing_premium_views_data() {
  $data['listing_premium_levels']['table']['group']  = t('Premium Listings');
  $data['listing_premium_nodes']['table']['group']  = t('Premium Listings');
  //
  $data['listing_premium_levels']['table']['join'] = array(
	'node' => array(
	  'left_table' => 'listing_premium_nodes',
	  'left_field' => 'lpid',
	  'field' => 'lpid'
	),
    'listing_premium_nodes' => array(	  
      'left_field' => 'lpid',
      'field' => 'lpid',
    ),
  );
  $data['listing_premium_nodes']['table']['join'] = array(
	'node' => array(
	  'left_field' => 'nid',
	  'field' => 'nid'
	),
  );
  $data['listing_premium_levels']['weight'] = array(
   'title' => t('Listing Level Weight'),
    'help' => t('The weight applied to the listing level.'),
	'operator' =>  array('=' => 'equals'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['listing_premium_nodes']['lpid'] = array(
   'title' => t('Listing Level ID'),
    'help' => t('The ID of the listing level applied to the node.'),
	'operator' =>  array('=' => 'equals'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
  );
  $data['listing_premium_nodes']['oid'] = array(
   'title' => t('Attribute option ID'),
    'help' => t('The ID of the attribute option applied to the node.'),
	'operator' =>  array('=' => 'equals'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
  );
  return $data;
}
