<?php // $Id: listing_premium.pages.inc,v 1.0 2011/03/17 09:00:32 newzeal Exp $
/*
 *  Displays how the node will be seen as a listing and as a node page for each of the listing levels
 */
function listing_premium_preview($node) {
  $product_nid = db_result(db_query("SELECT product_nid FROM {uc_node_checkout_types} WHERE node_type='%s'", $node->type));
  $result = db_query("SELECT lpid, name FROM {listing_premium_levels} WHERE product_nid=%d", $product_nid);
  $levels = array();
  while ($row = db_fetch_object($result)) {
    $levels[$row->lpid] = $row->name;
  }
  $n = $node;
  $output = '';
  //
  foreach ($levels as $lpid => $level) {
    $lpid_node = $n;
    $lpid_node->listing_premium['lpid'] = $lpid;
	$lpid_node->nid = $lpid_node->nid * $lpid;
	//  In order to bypass hook_field_access in listing_premium.module we add a flag here
	$lpid_node->listing_premium['mode'] = 'preview';
	unset ($lpid_node->title); // we don't need the title
	//  node_build content appears to cache the '#children' array of the built node in a manner which cannot be bypassed by unsetting the node in this loop

	//  The following code is taken from node_view and we ensure that both '#printed' and '#children' are unset before we render
	//  The render function will render '#children' if it exists, otherwise it generates '#children' via the relevant fields
    $lpid_node = node_build_content($lpid_node, $teaser, $page);
    if ($links) {
      $lpid_node->links = module_invoke_all('link', 'node', $lpid_node, $teaser);
      drupal_alter('link', $lpid_node->links, $lpid_node);
    }
    // Set the proper node part, then unset unused $node part so that a bad
    // theme can not open a security hole.
    unset($lpid_node->content['#printed']);
    unset($lpid_node->content['#children']);
    $content = drupal_render($lpid_node->content);
    $lpid_node->body = $content;
    unset($lpid_node->teaser);
    // Allow modules to modify the fully-built node.
    node_invoke_nodeapi($lpid_node, 'alter');
    $view = theme('node', $lpid_node);	

	$output .= theme('listing_premium_preview', $lpid, $level, $view);
	unset($lpid_node);
  }
  if ($output == '') return t('There are no levels set for this content type in <a href="/admin/store/settings/node-checkout/overview">uc node checkout settings</a>.');
  else return $output;
}

function autheal_render(&$elements) {
  if (!isset($elements) || (isset($elements['#access']) && !$elements['#access'])) {
    return NULL;
  }
drupal_set_message('here 1');
  // If the default values for this element haven't been loaded yet, populate
  // them.
  if (!isset($elements['#defaults_loaded']) || !$elements['#defaults_loaded']) {
    if ((!empty($elements['#type'])) && ($info = _element_info($elements['#type']))) {
      $elements += $info;
    }
  }

  // Make any final changes to the element before it is rendered. This means
  // that the $element or the children can be altered or corrected before the
  // element is rendered into the final text.
  if (isset($elements['#pre_render'])) {
    foreach ($elements['#pre_render'] as $function) {
      if (function_exists($function)) {
        $elements = $function($elements);
      }
    }
  }

  $content = '';
  // Either the elements did not go through form_builder or one of the children
  // has a #weight.
  if (!isset($elements['#sorted'])) {
    uasort($elements, "element_sort");
  }
  $elements += array(
    '#title' => NULL,
    '#description' => NULL,
  );
  drupal_set_message('here 2');
  if (!isset($elements['#children'])) {
    $children = element_children($elements);
    drupal_set_message('children '.print_r($children,1));
    // Render all the children that use a theme function.
    if (isset($elements['#theme']) && empty($elements['#theme_used'])) {
      $elements['#theme_used'] = TRUE;

      $previous = array();
      foreach (array('#value', '#type', '#prefix', '#suffix') as $key) {
        $previous[$key] = isset($elements[$key]) ? $elements[$key] : NULL;
      }
      // If we rendered a single element, then we will skip the renderer.
      if (empty($children)) {
        $elements['#printed'] = TRUE;
      }
      else {
        $elements['#value'] = '';
      }
      $elements['#type'] = 'markup';

      unset($elements['#prefix'], $elements['#suffix']);
      $content = theme($elements['#theme'], $elements);

      foreach (array('#value', '#type', '#prefix', '#suffix') as $key) {
        $elements[$key] = isset($previous[$key]) ? $previous[$key] : NULL;
      }
    }
    // Render each of the children using drupal_render and concatenate them.
    if (!isset($content) || $content === '') {
      foreach ($children as $key) {
        $content .= drupal_render($elements[$key]);
      }
    }
  }
  if (isset($content) && $content !== '') {
    $elements['#children'] = $content;
  }

  // Until now, we rendered the children, here we render the element itself
  if (!isset($elements['#printed'])) {
    drupal_set_message('here 3 '.print_r($elements['field_list_level']['field']['#title'],1));
    $content = theme(!empty($elements['#type']) ? $elements['#type'] : 'markup', $elements);
    $elements['#printed'] = TRUE;
  }

  if (isset($content) && $content !== '') {
    // Filter the outputted content and make any last changes before the
    // content is sent to the browser. The changes are made on $content
    // which allows the output'ed text to be filtered.
	drupal_set_message('here 4');
    if (isset($elements['#post_render'])) {
      foreach ($elements['#post_render'] as $function) {
        if (function_exists($function)) {
          $content = $function($content, $elements);
        }
      }
    }
    $prefix = isset($elements['#prefix']) ? $elements['#prefix'] : '';
    $suffix = isset($elements['#suffix']) ? $elements['#suffix'] : '';
    return $prefix . $content . $suffix;
  }
}