/* 
 *   Listings.js jQuery functions  newzeal
 */
function listing_show_term_select(tid) {
    show_list_throbber();
	
    var type = $('#edit-select-type').val();
	
	if(type == 'all') {
	  $('#edit-select-term').empty();
	  $('#edit-select-term-wrapper').attr('style', 'visibility: hidden');
	}
	else {
      $.post(Drupal.settings.basePath + '?q=listings/terms/js/' + type + '/' + tid, { },
         function(contents) {
           if (contents != '') {
             $('#edit-select-term').empty().append(contents);
           }
		   if(type!='all') $('#edit-select-term-wrapper').attr('style', 'visibility: visible');
           hide_list_throbber();
         }
      );
	}
}
function getListings(context,block_id) {
    $("input[@id*=search-button]:not(.getSearch-processed)", context).addClass('getSearch-processed').click(function() {
      // returns false to prevent default actions and propogation	  
 	  var search_value = $('#edit-select-target').val();
	  doSearch(search_value,1);

     return false;
    });
}
function doSearch(search_value,block_id) {
	show_list_throbber();
	
    $.post(Drupal.settings.basePath + '?q=autheal/get_listings/' + search_value + '/' + block_id, { },
         function(contents) {
           if (contents != '') {
             $('#targeted-list').empty().append(contents);
           }
           hide_list_throbber();
         }
    );

}
function show_list_throbber() {
  $('#search-div-throbber').attr('style', 'background-image: url(' + Drupal.settings.basePath + 'misc/throbber.gif); background-repeat: no-repeat; background-position: 100% -20px;').html('&nbsp;&nbsp;&nbsp;&nbsp;');
}

function hide_list_throbber() {
  $('#search-div-throbber').removeAttr('style').empty();
}