<?php

/**
 * Implementation of hook_views_handlers().
 */
function listing_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'listing') .'/views',
    ),
    'handlers' => array(
      'listing_handler_filter_node_type' => array(
        'parent' => 'views_handler_filter_in_operator',
      ),
    ),
  );
}

/**
 * Implementation of hook_views_data_alter().
 */

function listing_views_data_alter(&$data) {
  
  $data['node']['listing_type'] = array(
    'title' => t('Listing Type'), // The item it appears as on the UI,
    'help' => t('The type of a listing.'), // The help that appears on the UI,	
    'filter' => array(
      'handler' => 'listing_handler_filter_node_type',
	  'field' => 'type',
    ),
  );
  
}