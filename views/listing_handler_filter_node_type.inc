<?php
/**
 * Filter by node type
 */
class listing_handler_filter_node_type extends views_handler_filter_in_operator {
  function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t('Listing type');
      $types = variable_get('listing_content_types', array());
      foreach ($types as $type => $name) {
        $options[$type] = $name;
      }
      $this->value_options = $options;
    }
  }
}