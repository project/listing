<?php
function listing_role_settings_form() {
  $form['listing_role_node_view_alert'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Node View Alert'),
    '#default_value' => variable_get('listing_role_node_view_alert', 1),
    '#description'   => t("Display a link for the owner of a listing node on the view page to the role based listings application page if the node is not already upgraded."),
  );

  $form['#validate'][] = 'listing_role_settings_validate';
  return system_settings_form($form);
}
function listing_role_settings_validate($form, $form_state) {
  if (!is_numeric($form_state['values']['listing_role_levels'])) form_set_error('listing_role_levels', t('The number of levels has to be numeric'));
}

function listing_role_types_admin() {
  $output = l('Add a Role Based Listing Type', 'admin/listing/roles/types/add');
  $result = db_query("SELECT * FROM {listing_role_types}");
  $rows = array();
  $header = array('Name', 'Description', 'Operation');
  while ($row = db_fetch_object($result)) {
    $rows[$row->lrid]['name'] = $row->name;
    $rows[$row->lrid]['description'] = $row->description;
    $rows[$row->lrid]['edit'] = l('Edit', 'admin/listing/roles/types/'.$row->lrid.'/edit');
  }
  $output .= theme('table', $header, $rows);
  return $output;
}
function listing_role_type_form($form_state, $listing_type=NULL) {
  $form['name'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Name'),
      '#default_value' => isset($listing_type->name) ? $listing_type->name : '',
      '#description'   => t("The name of this role based listing level."),
  );
  $form['description'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Description'),
      '#default_value' => isset($listing_type->description) ? $listing_type->description : '',
      '#description'   => t("The description for this role based listing level."),
  );
  $options = array();
  foreach (user_roles() as $rid => $role) {
    //drupal_set_message(print_r($role,1));
	$options[$rid] = $role;
  }
  $form['role_rid'] = array(
      '#type'          => 'select',
	  '#options' 	   => $options,
      '#title'         => t('Role'),
      '#default_value' => isset($listing_type->role_rid) ? $listing_type->role_rid : array(),
      '#description'   => t("The role to which this role based listing type applies."),
  );
  $options = array();
  foreach (content_fields() as $field) {
    //drupal_set_message(print_r($field,1));
	$options[$field['field_name']] = $field['widget']['label']. ' (' .$field['type_name']. ')';
  }
  $form['data'] = array(
      '#type'          => 'select',
	  '#options' 	   => $options,
	  '#multiple'	   => TRUE,
      '#title'         => t('Fields'),
      '#default_value' => isset($listing_type->data) ? unserialize($listing_type->data) : array(),
      '#description'   => t("Whichever fields you select here will not display unless the user has the selected role."),
  );
  $form['lrid'] = array(
      '#type'          => 'hidden',
      '#value'         => isset($listing_type->lrid) ? $listing_type->lrid : 0,
  );
  $form['save'] = array(
      '#type'          => 'submit',
      '#value'         => t('Save'),
  );
  $form['delete'] = array(
      '#type'          => 'submit',
      '#value'         => t('Delete'),
  );
  return $form;
}
function listing_role_type_form_submit($form, $form_state) {
  //drupal_set_message(print_r($form_state['values'],1));
  if ($form_state['values']['op'] == "Save") {
    if ($form_state['values']['lrid'] > 0) {
      db_query("UPDATE {listing_role_types} SET name='%s', description='%s', role_rid=%d, data='%s' WHERE lrid=%d", $form_state['values']['name'], $form_state['values']['description'], $form_state['values']['role_rid'], serialize($form_state['values']['data']), $form_state['values']['lrid']);
	  $lrid = $form_state['values']['lrid'];
    }
    else {
      db_query("INSERT INTO {listing_role_types} (name, description, role_rid, data) VALUES ('%s', '%s', %d, '%s')", $form_state['values']['name'], $form_state['values']['description'], $form_state['values']['role_rid'], serialize($form_state['values']['data']));
	  $lrid = db_last_insert_id('listing_role_types', 'lrid');
    }
    db_query("DELETE FROM {listing_role_fields} WHERE lrid=%d", $lrid);
    foreach ($form_state['values']['data'] as $field) {
      db_query("INSERT INTO {listing_role_fields} (field_name, lrid, active) VALUES ('%s', %d, 1)", $field, $lrid);
    }
  }
  drupal_goto('admin/listing/roles/types');
}