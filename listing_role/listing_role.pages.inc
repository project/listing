<?php
function listing_role_content() {
  // First we check if this node is currently enhanced
  $result = db_query("SELECT n.*, t.name, t.description FROM {listing_role_nodes} n LEFT JOIN {listing_role_types} t ON n.lpid=t.lpid WHERE n.nid=%d", arg(1));
  $i = 0;
  $header = array(t('Name'), t('Description'), t('Start'), t('Expiry'));
  $rows = array();
  while ($row = db_fetch_object($result)) {
    $rows[$i]['name'] = $row->name;
	$rows[$i]['description'] = $row->description;
    $rows[$i]['created'] = format_date($row->created);
    $rows[$i]['expiry'] = format_date($row->expiry);
    $i++;
  }
  if (!empty($rows)) $output = theme('table', $header, $rows);
  // Now we display the options for making this a role based listing
  $header = array('Name', '');
  $rows = array();
  $result = db_query("SELECT * FROM {listing_role_types}");
 
  while ($row = db_fetch_object($result)) {
    $node = node_load($row->product_nid);
    $rows[$row->lpid]['name'] = $row->name;
	$rows[$row->lpid]['add-to-cart'] = theme('uc_product_add_to_cart', $node);
	
  }
  $output .= theme('table', $header, $rows);
  return $output;
}

