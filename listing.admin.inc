<?php
/**
 *  Admin settings().
 */
function listing_admin_settings_form() {
  $form['listing_url'] = array(
    '#type'          => 'textfield',
    '#title'         => t('The url to use for listings'),
	'#description'   => t('Enter a string to appear at the beginning of all listing views created by this module.  Do not begin or end with a /'),
    '#default_value' => variable_get('listing_url', 'listing-search'),
  );
  $form['listing_province_name'] = array(
    '#type'          => 'textfield',
    '#title'         => t('The text to use for province'),
	'#description'   => t('Enter the label you wish to use when referring to states or provinces'),
    '#default_value' => variable_get('listing_province_name', 'province'),
  );
  $options = node_get_types('names');
  $form['listing_content_types'] = array(
    '#type'          => 'select',
	'#options'		 => $options,
	'#multiple' 	 => TRUE,
    '#title'         => t('Content types used by listing module.'),
    '#default_value' => variable_get('listing_content_types', array())
  );
  $form['listing_debug'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Debug Listings.'),
    '#description'   => t('Turn on debug messages in all listing modules listings'),
    '#default_value' => variable_get('listing_debug', 0)
  );
  return system_settings_form($form);
}