<?php
function listing_node_publish($node, $op) {
  if (node_access('update', $node)) {
    switch ($op) {
      case 'publish':
	    //  If we are running scheduler we need to check if this Listing has been permanently unpublished
		if (!listing_scheduler_check($node)) {
		  drupal_set_message(t('This listing has expired.'));
		  return;
		}
        $node->status = 1;
        node_save($node);
        drupal_set_message(t('Listing published'));
        break;
      case 'unpublish':
        $node->status = 0;
        node_save($node);
        drupal_set_message(t('Listing unpublished'));
    }
  }
  drupal_goto('node/'. $node->nid .'/listing');
}
function listing_node_delete($node) {
  drupal_goto('node/'.$node->nid.'/delete');
}
/*
 *   Create the info module from other modules
 */
function listing_info_page($node) {
  if ($node->status == 1) $status = t('Published');
  else $status = t('Unpublished');
  $output = '<div class="listing-info">'. t('This listing is currently !status', array('!status' => $status)) .'</div>';
  foreach (module_implements('listing_author_info') as $module) {
    $func = $module .'_listing_author_info';
	//$output .= '<div class="listing-info-module">'. ucfirst(str_replace("_", " ", $module)) .'</div>';
    $output .= $func($node);
  }
  return $output;
}