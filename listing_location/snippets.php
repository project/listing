<?php
//  Not sure if this is being used at all
function listing_location_location_city_select($el) {
  $result = db_query("SELECT city, state FROM {zipcodes} ORDER BY city ASC");
  while ($row = db_fetch_object($result)) {
    $cities[$row->city] = $row->city;
  }
  $element['city]'] = array(
    '#type' => 'select',
	'#options' => $cities,
    '#title' => t('City'),
    '#maxlength' => 60,
    '#value' => $el['#default_value']['city'],
    '#weight' => -2,
	'#name' => 'locations[0][city]',
	'#id' => 'edit-locations-0-city',
  );
  return drupal_render($element);
}
// Not sure if this is doing the right thing
function listing_location_get_targeted_search_location_text() {
  if (is_numeric($_SESSION['listings']['targeted_search'.$save])) {
     $data = db_fetch_array(db_query("SELECT city, state as code FROM {zipcodes} WHERE zip=%d", $_SESSION['listings']['targeted_search'.$save]));
  }
  else {
    $parts = explode(":",$_SESSION['listings']['targeted_search'.$save]);
    $data['city'] = $parts[0];
    $data['code'] = trim(strtoupper($parts[1]));
  }
  $states = listing_location_get_states();
  $data['state'] = $states[$data['code']];
  return $data;

}
