<?php
function us_map() {
return '
<div id="usamap" style="height: 247px; width: 400px; "><map name="ImageMap_1_1948960998" id="ImageMap_1_1948960998">
<area href="'.base_path().LISTING_URL.'/us/alabama" shape="poly" coords="268,155,278,188,259,191,259,197,253,195,252,156" alt="Alabama" title="Alabama" />
<area href="'.base_path().LISTING_URL.'/us/alaska" shape="poly" coords="63,191,64,225,74,228,85,240,85,246,76,245,66,231,49,228,42,236,5,241,28,228,17,216,22,196,30,187,45,182" alt="Alaska" title="Alaska" />
<area href="'.base_path().LISTING_URL.'/us/arizona" shape="poly" coords="55,165,79,181,95,184,102,135,67,129,65,136,62,136,58,145,63,152" alt="Arizona" title="Arizona" />
<area href="'.base_path().LISTING_URL.'/us/arkansas" shape="poly" coords="204,144,204,168,208,175,230,175,236,155,239,147,233,145" alt="Arkansas" title="Arkansas" />
<area href="'.base_path().LISTING_URL.'/us/california" shape="poly" coords="11,68,38,75,30,101,58,141,58,144,62,151,56,164,38,162,17,139,5,81" alt="California" title="California" />
<area href="'.base_path().LISTING_URL.'/us/colorado" shape="poly" coords="107,98,154,103,152,138,103,133" alt="Colorado" title="Colorado" />
<area href="'.base_path().LISTING_URL.'/us/connecticut" shape="poly" coords="341,77,341,86,352,81,351,75" alt="Connecticut" title="Connecticut" />
<area href="'.base_path().LISTING_URL.'/us/delaware" shape="poly" coords="331,100,333,113,339,113,337,106" alt="Delaware" title="Delaware" />
<area href="'.base_path().LISTING_URL.'/us/florida" shape="poly" coords="305,189,324,227,316,243,308,243,298,217,287,197,276,202,260,196,260,192,278,189,278,192" alt="Florida" title="Florida" />

<area href="'.base_path().LISTING_URL.'/us/georgia" shape="poly" coords="268,153,286,152,307,174,304,190,278,191" alt="Georgia" title="Georgia" />
<area href="'.base_path().LISTING_URL.'/us/hawaii" shape="poly" coords="91,211,90,230,108,245,141,246,143,232,122,212" alt="Hawaii" title="Hawaii" />
<area href="'.base_path().LISTING_URL.'/us/idaho" shape="poly" coords="70,20,64,45,67,48,60,58,57,79,94,86,98,65,86,64,83,53,79,55,80,44,73,30,76,21" alt="Idaho" title="Idaho" />
<area href="'.base_path().LISTING_URL.'/us/illinois" shape="poly" coords="231,92,225,110,235,121,233,127,243,138,248,137,252,121,251,95,246,87,230,90" alt="Illinois" title="Illinois" />
<area href="'.base_path().LISTING_URL.'/us/indiana" shape="poly" coords="251,95,251,130,263,127,271,118,268,93" alt="Indiana" title="Indiana" />
<area href="'.base_path().LISTING_URL.'/us/iowa" shape="poly" coords="192,82,189,90,196,108,223,108,227,102,231,94,225,83" alt="Iowa" title="Iowa" />
<area href="'.base_path().LISTING_URL.'/us/kansas" shape="poly" coords="153,112,150,139,203,140,203,121,201,119,202,115,196,112" alt="Kansas" title="Kansas" />
<area href="'.base_path().LISTING_URL.'/us/kentucky" shape="poly" coords="242,143,282,138,291,129,284,120,272,117,264,129,251,130" alt="Kentucky" title="Kentucky" />
<area href="'.base_path().LISTING_URL.'/us/louisiana" shape="poly" coords="210,203,213,191,208,183,209,176,231,176,231,181,228,192,239,192,244,198,247,208,228,210" alt="Louisiana" title="Louisiana" />
<area href="'.base_path().LISTING_URL.'/us/maine" shape="poly" coords="349,46,354,62,356,65,374,44,370,37,364,25,354,23" alt="Maine" title="Maine" />
<area href="'.base_path().LISTING_URL.'/us/Maryland" shape="poly" coords="305,105,305,110,315,106,324,111,324,115,329,116,333,123,338,112,332,112,331,101" alt="Maryland" title="Maryland" />
<area href="'.base_path().LISTING_URL.'/us/massachusetts" shape="poly" coords="340,71,341,78,354,75,357,78,366,79,366,71,356,67" alt="Massachusetts" title="Massachusetts" />
<area href="'.base_path().LISTING_URL.'/us/michigan" shape="poly" coords="227,48,231,55,248,62,256,93,277,91,281,80,270,53,242,35" alt="Michigan" title="Michigan" />
<area href="'.base_path().LISTING_URL.'/us/minnesota" shape="poly" coords="187,33,190,60,189,61,192,65,192,82,226,82,226,78,214,70,213,63,217,53,232,40,201,36,199,30" alt="Minnesota" title="Minnesota" />
<area href="'.base_path().LISTING_URL.'/us/mississippi" shape="poly" coords="237,157,251,155,253,197,244,197,239,191,228,191,232,178,231,173" alt="Mississippi" title="Mississippi" />
<area href="'.base_path().LISTING_URL.'/us/missouri" shape="poly" coords="196,108,196,112,203,120,203,144,236,143,234,147,238,147,243,139,233,127,234,123,223,109" alt="Missouri" title="Missouri" />
<area href="'.base_path().LISTING_URL.'/us/montana" shape="poly" coords="77,20,75,29,81,43,79,53,83,51,87,63,98,64,99,61,143,66,146,31" alt="Montana" title="Montana" />

<area href="'.base_path().LISTING_URL.'/us/nebraska" shape="poly" coords="142,85,140,102,153,103,153,110,198,112,192,90,178,87" alt="Nebraska" title="Nebraska" />
<area href="'.base_path().LISTING_URL.'/us/nevada" shape="poly" coords="38,76,75,83,65,135,61,135,59,143,30,102" alt="Nevada" title="Nevada" />
<area href="'.base_path().LISTING_URL.'/us/new-hampshire" shape="poly" coords="346,48,344,70,355,67,348,45" alt="New Hampshire" title="New Hampshire" />
<area href="'.base_path().LISTING_URL.'/us/new-jersey" shape="poly" coords="334,86,333,94,336,96,332,100,337,106,343,93,340,91,340,88" alt="New Jersey" title="New Jersey" />
<area href="'.base_path().LISTING_URL.'/us/new-mexico" shape="poly" coords="102,133,145,138,140,182,102,180,101,185,94,183" alt="New Mexico" title="New Mexico" />
<area href="'.base_path().LISTING_URL.'/us/new-york" shape="poly" coords="303,74,298,85,329,79,333,85,340,87,340,90,343,92,353,84,342,85,341,77,341,70,335,52,324,53,314,69" alt="New York" title="New York" />
<area href="'.base_path().LISTING_URL.'/us/north-carolina" shape="poly" coords="294,137,278,153,298,149,312,150,320,156,341,139,335,129" alt="North Carolina" title="North Carolina" />
<area href="'.base_path().LISTING_URL.'/us/north-dakota" shape="poly" coords="146,32,144,58,190,60,186,33" alt="North Dakota" title="North Dakota" />
<area href="'.base_path().LISTING_URL.'/us/ohio" shape="poly" coords="272,117,283,119,286,122,298,101,296,89,281,93,276,92,269,93,271,115" alt="Ohio" title="Ohio" />
<area href="'.base_path().LISTING_URL.'/us/oklahoma" shape="poly" coords="145,138,203,140,206,170,179,166,166,162,166,145,144,144" alt="Oklahoma" title="Oklahoma" />
<area href="'.base_path().LISTING_URL.'/us/oregon" shape="poly" coords="25,33,11,60,11,68,57,78,59,57,65,49,63,43,41,43,30,40,30,35" alt="Oregon" title="Oregon" />
<area href="'.base_path().LISTING_URL.'/us/pennsylvania" shape="poly" coords="295,88,298,106,330,100,335,96,332,93,333,85,329,80" alt="Pennsylvania" title="Pennsylvania" />
<area href="'.base_path().LISTING_URL.'/us/rhode-island" shape="poly" coords="354,75,356,78,353,81,351,76" alt="Rhode Island" title="Rhode Island" />
<area href="'.base_path().LISTING_URL.'/us/south-carolina" shape="poly" coords="286,151,308,174,319,157,312,150,298,148" alt="South Carolina" title="South Carolina" />
<area href="'.base_path().LISTING_URL.'/us/south-dakota" shape="poly" coords="144,58,190,60,188,62,191,65,191,81,190,89,177,86,156,84,142,84,143,76,143,67" alt="South Dakota" title="South Dakota" />
<area href="'.base_path().LISTING_URL.'/us/tennessee" shape="poly" coords="237,156,278,153,294,136,240,143" alt="Tennessee" title="Tennessee" />
<area href="'.base_path().LISTING_URL.'/us/texas" shape="poly" coords="144,143,140,183,114,180,128,202,138,209,145,202,153,203,170,233,183,238,183,222,211,204,213,191,208,172,201,168,166,162,165,144" alt="Texas" title="Texas" />

<area href="'.base_path().LISTING_URL.'/us/utah" shape="poly" coords="76,84,95,87,93,97,107,98,102,134,68,128" alt="Utah" title="Utah" />
<area href="'.base_path().LISTING_URL.'/us/vermont" shape="poly" coords="335,51,340,70,344,70,345,50" alt="Vermont" title="Vermont" />
<area href="'.base_path().LISTING_URL.'/us/virginia" shape="poly" coords="291,129,282,138,335,128,329,117,323,115,324,111,319,108,314,108,305,115,303,127" alt="Virginia" title="Virginia" />
<area href="'.base_path().LISTING_URL.'/us/washington" shape="poly" coords="24,32,30,35,30,40,46,43,66,45,70,20,37,10,25,14" alt="Washington" title="Washington" />
<area href="'.base_path().LISTING_URL.'/us/west-virginia" shape="poly" coords="318,108,314,108,310,116,305,115,303,124,299,128,292,130,287,124,289,115,296,109,297,100,298,107,306,106,306,109,314,105,317,106" alt="West Virginia" title="West Virginia" />
<area href="'.base_path().LISTING_URL.'/us/wisconsin" shape="poly" coords="218,54,217,58,214,61,214,70,224,77,225,88,229,90,247,88,250,65,247,64,244,60,228,53,224,50" alt="Wisconsin" title="Wisconsin" />
<area href="'.base_path().LISTING_URL.'/us/wyoming" shape="poly" coords="100,62,93,97,140,102,143,65" alt="Wyoming" title="Wyoming" />
<area href="'.base_path().LISTING_URL.'/us/delaware" shape="rect" coords="368,113,397,121" alt="Delaware" title="Delaware" />
<area href="'.base_path().LISTING_URL.'/us/maryland" shape="rect" coords="368,121,396,131" alt="Maryland" title="Maryland" />
<area href="'.base_path().LISTING_URL.'/us/new-hampshire" shape="rect" coords="281,14,327,25" alt="New Hampshire" title="New Hampshire" />
<area href="'.base_path().LISTING_URL.'/us/new-jersey" shape="rect" coords="361,102,396,112" alt="New Jersey" title="New Jersey" />
<area href="'.base_path().LISTING_URL.'/us/massachusetts" shape="rect" coords="283,37,323,47" alt="Massachusetts" title="Massachusetts" />
<area href="'.base_path().LISTING_URL.'/us/connecticut" shape="rect" coords="363,93,398,101" alt="Connecticut" title="Connecticut" />
<area href="'.base_path().LISTING_URL.'/us/west-virginia" shape="rect" coords="358,142,397,150" alt="West Virginia" title="West Virginia" />
<area href="'.base_path().LISTING_URL.'/us/vermont" shape="rect" coords="303,26,325,36" alt="Vermont" title="Vermont" />
<area href="'.base_path().LISTING_URL.'/us/rhode_island" shape="rect" coords="360,81,398,92" alt="Rhode Island" title="Rhode Island" /></map><img alt="Map of USA with state names" src="'. base_path() . drupal_get_path('module', 'listing_location').'/maps/us_map.png" width="400" height="247" usemap="#ImageMap_1_1948960998" />
</div>';
}