<?php

/*
 *  Generate the ListingsMap and ListingsList views with the requested location  and taxonomy values
 *  Also do url validation
 */
function listing_location_search_view() {
  global $LOC;
  //drupal_set_message('Session Process: session variables being processed: '.print_r($_SESSION['listings'],1));
  if (variable_get('listing_location_countries', 0)) {
    if (!variable_get('listing_location_views', 0)) {
	  return t('The required views for this module are not properly installed.  Please check instructions', 'error');
    }

    // First we get the current arguments in the url and check that the views exist
    $args = _listing_location_analyze_url();
    //  There are some instances whereby we are presented with this page and there are no arguments in the url, such as when LISTING_URL is set as the home page so we revert to the most recently stored arguments or the default
	if ($args['zip'] != "zip" && $args['city']=='') $args = listing_location_args_revert();
	//  There is no type in the url and we have selected to have a special city page so we will display that instead of the view
	if ($args['type'] == '' && module_exists('listing_location_city') && variable_get('listing_location_city_page', 0)) {
	  return listing_location_city_page($args);
	}
	
	//  If this is a first time visit and there is no url then also distance and type may not be set
	if (!$args['distance'] > 0) $args['distance'] = variable_get('listing_location_target_default_distance', 25);
    //  We use either the city select search form and view or the location search form and view depending on what is selected
    $views = array();
	
    if (variable_get('listing_location_include_map', 1)) {
      if (variable_get('listing_location_select_search', 0)) $views[] = 'ListingsMapbyCity';
      else $views[] = 'ListingsMap';
    }
    // When we use the city select option we do not need the proximity filter but use a different filter
    if (variable_get('listing_location_select_search', 0)) $views[] = 'ListingsListbyCity';
    else $views[] = 'ListingsList';
    if (variable_get('listing_location_select_search', 0)) $return['form'] = drupal_get_form('listing_location_select_search_form');
    else $return['form'] = drupal_get_form('listing_location_proximity_search_form');
    $list = '';
    foreach ($views as $view_name) {
	  //  We find out if we should use a particular view display
      if (($view_name=="ListingsListbyCity" || $view_name=="ListingsList") && variable_get('listing_location_view_per_ctype', 0)) {
        $display_id = variable_get('listing_location_view_display_'.$args['type'], '');
		
      }
	  else $display_id = 'default';  // Map views always use default.  
      $view = views_get_view($view_name);
	  // Add a hook so that other modules can modify the view before we modify anything
	  foreach (module_implements('listing_location_search_preview') as $module) {
	    $func = $module.'_listing_location_search_preview';
	    $func($view);
	  }
	  if ($view->name=="ListingsListbyCity" || $view->name=="ListingsMapbyCity") {
	    if ($_SESSION['listings']['targeted_search'] == 'all-cities') {
	      unset($view->display[$display_id]->display_options['filters']['city']);
	    } else {
	      $parts = explode(': ', $_SESSION['listings']['targeted_search']);
	      $view->display[$display_id]->display_options['filters']['city']['value'] = $parts[0];
	    }
	  }
	  
	  if ($view->name=="ListingsMap" || $view->name=="ListingsList") {
	    if ($args['zip']=="zip" && variable_get('listing_location_include_zipcode', 0)) {
	      $result = db_query("SELECT latitude, longitude FROM {zipcodes} WHERE zip=%d", intval($_SESSION['listings']['targeted_search']));		
		}
	    //  For the map we zoom on the search center of focus
		else {
	      if ($_SESSION['listings']['targeted_search'] == 'all-cities') $parts = explode(":", variable_get('listing_default_location', 'Hastings: HKB'));
	      else $parts = explode(":", $_SESSION['listings']['targeted_search']);
	      $result = db_query("SELECT latitude, longitude FROM {zipcodes} WHERE LOWER(city)='%s' AND LOWER(state)='%s' AND LOWER(country)='%s'", strtolower(trim($parts[0])), strtolower(trim($parts[1])), strtolower($_SESSION['listings']['country']));
		}
	    $_SESSION['listings']['distance_units'] = $view->display[$display_id]->display_options['filters']['targeted_proximity']['value']['search_units'];
		$args['distance_units'] = $view->display[$display_id]->display_options['filters']['targeted_proximity']['value']['search_units'];
	    $view->display[$display_id]->display_options['filters']['targeted_proximity']['value']['search_distance'] = $args['distance'];
	    $target = db_fetch_object($result);
		if ($args['distance'] > variable_get('listing_location_map_zoom_threshold', 75)) $zoom = variable_get('listing_location_map_zoom', 6) - 1;
		else $zoom = variable_get('listing_location_map_zoom', 6);
	    if (!empty($target->latitude) && !empty($target->longitude)) {
	      $view->display[$display_id]->display_options['style_options']['macro'] = '[gmap zoom='. $zoom .' |center='.$target->latitude.','.$target->longitude.' |width='.variable_get('listing_location_map_width', 300).'px |height='.variable_get('listing_location_map_height', 200).'px |control=Small |type=Map]';
	    }
	  }
	  // When using zipcodes we do this
	  if ($args['zip']=="zip" && variable_get('listing_location_include_zipcode', 0)) {
	    // Country validation
		 
	    /*
	  if (arg(1)!='us') {
	    drupal_set_message('The database currently only supports US based listings');
	    drupal_goto(LISTING_URL.'/specialty/all');
	  }
	  */
	    if ($args['type']=="all") {
	      foreach (variable_get('listing_location_content_types', array()) as $name => $type) {
	        $view->display[$display_id]->display_options['filters']['type']['value'][$name] = $name;
		  }
		  $type = 'all';
	    }
	    else {
	      $type = db_result(db_query("SELECT type FROM {node_type} WHERE LOWER(name)='%s'", str_replace("-"," ", $args['type'])));
	      $view->display[$display_id]->display_options['filters']['type']['value'] = array($type => $type);
		  $type = str_replace("-"," ", $args['type']);  // use later for page title
	    }
	    if ($args['type']=="all" || $args['type']=='') {
	      if ($view_name=="ListingsMap" || $view_name=="ListingsMapbyCity") $list = listing_location_category_list();
	      unset($view->display[$display_id]->display_options['filters']['tid']);
	    }
	    else {
	      if ($LOC->tid>0) $view->display['default']->display_options['filters']['tid']['value'][0] = $LOC->tid;
	    }
	    if (is_numeric($args['term']))  {
	      $view->display[$display_id]->display_options['filters']['targeted_proximity']['value']['search_distance'] = $args['term'];
	    }
	    $zip = TRUE;
		
		$view->display[$display_id]->display_options['filters']['targeted_proximity']['value']['country'] = $_SESSION['listings']['country'];
	    $location = $_SESSION['listings']['targeted_search'];
	    // this needs to happen after the distance has been calculated to ensure that these listings have the same distance calculation as the view
	  }
	  // when not using zipcode we do this
	  else {
	    
	    //if (arg(1)=="reset") return 'location has been reset';
        if (variable_get('listing_location_show_province_url', 1) && !isset($args['state'])) {
	      $countries = location_get_iso3166_list();
	      drupal_set_title(t('!states in !country', array('!states' => ucfirst(variable_get('listing_province_name', 'province')).'s', '!country' => $_SESSION['listings']['country'])));
	      return listing_location_state_list($args['country']);
	    }
	  
	    if (!isset($args['city']) || $args['city']=='') {
	      //return listing_location_cities_by_state_block(arg(2));
	    }
	  
	    if ($args['type']=="all") {
		  foreach (variable_get('listing_location_ctypes', array()) as $name => $type) {
		    
	        $view->display[$display_id]->display_options['filters']['type']['value'][$name] = $name;
		  }
		  $type = 'all';
	    }
	    else {
	      $type = db_result(db_query("SELECT type FROM {node_type} WHERE LOWER(name)='%s'", str_replace("-"," ", $args['type'])));
	      $view->display[$display_id]->display_options['filters']['type']['value'] = array($type => $type);
		  $type = str_replace("-"," ", $args['type']);  // use later for page title
	    }
	  
	    if ($args['term']=="all" || $args['term']=='' || !isset($args['term'])) {
	      unset($view->display[$display_id]->display_options['filters']['tid']);
	    }
	    else {
	      if ($LOC->tid>0) $view->display[$display_id]->display_options['filters']['tid']['value'][0] = $LOC->tid;
	    }
	    if (is_numeric(arg(6)) && !variable_get('listing_location_select_search', 0))  {
	      $view->display[$display_id]->display_options['filters']['targeted_proximity']['value']['search_distance'] = arg(6);
	    }
	    $parts = explode(":", $_SESSION['listings']['targeted_search']);
	    $city_parts = explode(" ", $args['city']);
	    foreach ($city_parts as $key => $part) $city_parts[$key] = ucfirst($part);
	    $location['city'] = implode(" ",$city_parts);
	    if ($parts[0] == 'all-cities') $location['state'] = $args['state'];
	    else $location['state'] = strtoupper($parts[1]);
	    $location['state'] = $args['state'];
	    // this needs to happen after the distance has been calculated to ensure that these listings have the same distance calculation as the view
	    if ($args['term']=="all" || $args['term']=='' || !isset($args['term'])) {
	      if ($view_name=="ListingsMap" || $view_name=="ListingsMapbyCity") $list = listing_location_category_list();
	    }
	  }
	  // Add a hook so that other modules can modify the view after we have applied the programmatic filters
	  foreach (module_implements('listing_location_search_view') as $module) {
	    $func = $module.'_listing_location_search_view';
	    $func($view);
	  }
	  //This is what views uses
	  /*
          $output = $view->execute_display($display_id);
          vpr("Block $view->name execute time: " . (views_microtime() - $start) * 1000 . "ms");
          $view->destroy();
	  */
	  $output = $view->preview($display_id);
     // $view->execute($display_id);
	  if ($view_name=="ListingsMap" || $view_name=="ListingsMapbyCity") $return['map'] = $list.$output;
	  else $return['listings'] = $output;		
    }
	
    $weights = variable_get('listing_location_field_components', array('form' => 1, 'map' => 2, 'listings' => 3));
	asort($weights);
	$output = '';
	foreach ($weights as $component => $weight) {
	  $output .= $return[$component];
	}
	$args = listing_location_strings_from_args($args);
	$title = theme('listing_location_page_title', $args);
	drupal_set_title($title);

    return $output;
  }
  else return t('The listing location module only works when data is present in the location module zipcodes table and <a href="/admin/listing/location_search/general">listing location settings</a> have been applied.  Please read instructions.');
}

function listing_location_autocomplete($string = '') {
  global $country;
  $matches = array();
  if (is_numeric($string)) {
    print drupal_to_js('');
    exit();
  }
  else {
  /*
    $myFile = drupal_get_path('module', 'listing_location').'/country.txt';
    $fh = fopen($myFile, 'r');
    $country = fread($fh, 5);
    fclose($fh);
	*/
	if ($_SESSION['listings']['select_country']) $country = $_SESSION['listings']['select_country'];
	else $country = variable_get('listing_default_country', 'nz');
	$string = check_plain(str_replace("'", "", $string));
    $result = db_query("SELECT zip, city, state, country FROM {zipcodes} WHERE (LOWER(city) LIKE LOWER('%".$string."%') OR LOWER(city) LIKE LOWER('".$string."%')) AND country='".$country."'");
     while ($loc = db_fetch_object($result)) {
        $matches[$loc->city.': '.$loc->state] = check_plain($loc->city).', '.check_plain($loc->state).', '.check_plain($loc->country);
    }
  }
  print drupal_to_js($matches);
  exit();
}
function listing_location_country_change($country) {
  // We use a separate variable for this so that we do not upset out saved session
  $_SESSION['listings']['select_country'] = $country;
  /*
  $myFile = drupal_get_path('module', 'listing_location').'/country.txt';
  $fh = fopen($myFile, 'w') or die("can't open file");
  $stringData = $country;
  fwrite($fh, $stringData);
  fclose($fh);
  */
  print $country;
  exit();
}
function listing_location_clear() {
  unset($_SESSION['listings']);
  return 'Session variables cleared';
}
function listing_location_get_select_terms($type, $selected_tid) {
  $terms = listing_get_listing_term_names();
  $output = '';
  if (is_array($terms[$type])) {
    $output .= '<option value="all">All</option>';
    foreach ($terms[$type] as $tid => $term) {
	  if ($tid == $selected_tid)  $output .= '<option selected="selected" value="'.$tid.'">'.$term.'</option>';
      else $output .= '<option value="'.$tid.'">'.$term.'</option>';
    }
  }
  print $output;
  exit();
}

// This is a general error capture menu callback that returns the user to their saved session variables
function listing_location_listing_location_default() {
  drupal_set_message('Sorry the url you supplied appears to be invalid, please try again.');
  drupal_goto(listing_location_search_url('all', '_save'));
}

/*
 *  Featured listings
 */
function listing_location_featured_listings($targeted_search, $delta) {
  //  Loop through views
  if ($targeted_search == "default") {
    if ($_SESSION['listings']['targeted_search']) $targeted_search = $_SESSION['listings']['targeted_search'];
	else $targeted_search = variable_get('listing_default_location', 'Hastings: HKB');
  }
  $location = listing_location_extract_location($targeted_search);
  $target = db_fetch_array(db_query("SELECT latitude, longitude FROM {zipcodes} WHERE LOWER(city)='%s' AND LOWER(state)='%s'", strtolower($location['city']), strtolower($location['state'])));
  if (is_array($target)) $location = array_merge($location, $target);
  // we have a problem with new visitors who have no session cookie
  /*
  if (!isset($_SESSION['listings']['targeted_search'])) {
    $location = listing_location_get_geoip();
	$output['heading'] = l(t('Autism Service Providers near !city, !state', array('!city' => $location['city'], '!state' => strtoupper($location['code']))), listing_location_search_url());
  }
  */
  if (is_numeric($_SESSION['listings']['targeted_search'])) $output['heading'] = l(t('Autism Service Providers near zip code !zip', array('!zip' => $_SESSION['listings']['targeted_search'])), LISTING_URL.'/specialty/all');
  else $output['heading'] = l(listing_location_token_replace(variable_get('listing_location_featured_text', 'Service Providers near !city, !state'), array('!city' => $location['city'], '!state' => strtoupper($location['state']))), listing_location_search_url());
  for ($i=0; $i<variable_get('listing_location_featured_blocks', 2); $i++) {
    if ($count < variable_get('listing_location_featured_list_limit', 3)) {
	  $output['content'] .= listing_location_listing_location_view('block_'.$i, $count, $location);
    }
  }
  //if ($output['content'] == '') $output['content'] = t('No listings available');
  print drupal_json($output);
  exit();

}
function listing_location_token_replace($text, $variables) {
  foreach ($variables as $token => $variable) {
    $text = str_replace($token, $variable, $text);
  }
  return $text;
}
function listing_location_listing_location_view($display_id, &$count, $location) {
    $view = views_get_view('FeaturedListings');
    
    $diff = variable_get('listing_location_featured_list_limit', 3) - $count;
    $view->display[$display_id]->display_options['items_per_page'] = $diff;
   // $view->display[$display_id]->display_options['filters']['latitude'] = $location['latitude'];
    //$view->display[$display_id]->display_options['filters']['longitude'] = $location['longitude'];
	$output = $view->preview($display_id);
    $view->execute($display_id);
    $count = $count + count($view->result);
	if (variable_get('listing_location_debug', 0)) return '<b>Result from View'.$view_ind.'</b><br />'.count($view->result) .' results<br />'.$output;		
	elseif (count($view->result)>0 ) return $output;		 
	else return;
}
// Helper to extract city and state/province from the search string
function listing_location_extract_location($targeted_search) {
  static $location;
  if(!isset($location)) {
    $parts = explode(":", $targeted_search);
    $city_parts = explode(" ", $parts[0]);
    foreach ($city_parts as $key => $part) $city_parts[$key] = ucfirst($part);
    $location['city'] = implode(" ",$city_parts);   
    $location['state'] = trim(strtoupper($parts[1]));
  }
  return $location;
}
function listing_location_map($country) {
  switch($country) {
    case 'us':
	  require_once(drupal_get_path('module', 'listing_location').'/maps/us_map.tpl.php');
	  return us_map();
	  break;
	default:
	  return t('The country you selected is not in the database');
  }
  return 'no result';
}
