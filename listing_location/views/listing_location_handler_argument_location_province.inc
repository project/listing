<?php

/**
 * Argument handler to accept a province code or name.
 */
class listing_location_handler_argument_location_province extends location_handler_argument_location_province {
  /**
   * Override the argument changing from province name to code
   */
  function title() {
     return ucfirst($this->argument);
  }

  function query() {
	$provinces = location_get_provinces('us');
	$provinces = array_flip($provinces);
	$this->ensure_my_table();
    $placeholder = empty($this->definition['numeric']) ? "'%s'" : '%d';
    $this->query->add_where(0, "$this->table_alias.$this->real_field = $placeholder", $provinces[ucfirst(str_replace("-", " ",$this->argument))]);
  }
}
