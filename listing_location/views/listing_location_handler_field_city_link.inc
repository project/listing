<?php

/**
 * Field handler for terms.
 */
class listing_location_handler_field_city_link extends views_handler_field {
  // This needs work to remove redundant code
  function render($values) {
    $func = 'location_province_list_'.$_SESSION['listings']['country'];
    if (!function_exists($func)) include(drupal_get_path('module','location').'/supported/location.us.inc');
    $states = location_province_list_us();
    $state_name = strtolower(str_replace(" ","-", $states[$values->{location_province}]));
    return l($values->{$this->field_alias}, 'therapist/'.$values->{location_country}.'/'.$state_name.'/'.strtolower(str_replace(" ","-",$values->{$this->field_alias})).'/'.strtolower($values->{location_province}).'/all/25');
  
  }
}