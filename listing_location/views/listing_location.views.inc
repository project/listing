<?php 

/**
 * Implementation of hook_views_handlers().
 */
function listing_location_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'listing_location') .'/views',
    ),
    'handlers' => array(
      'listing_location_handler_argument_location_province' => array(
        'parent' => 'location_handler_argument_location_province',
      ),
      'listing_location_handler_filter_proximity' => array(
        'parent' => 'views_handler_filter',
      ),
      'listing_location_handler_sort_location_distance' => array(
        'parent' => 'views_handler_sort',
      ),
      'listing_location_handler_field_city_link' => array(
        'parent' => 'views_handler_field',
      ),     
    ),
  );
}

function listing_location_views_data() {
  $data['location']['province_name'] = array(
    'title' => t('Province Name'),
    'help' => t('The name of the province of the selected location.'),
    'argument' => array(
	  'field' => 'province',
      'handler' => 'listing_location_handler_argument_location_province',
      //'name field' => 'name',
    ),
  );
  $data['location']['city_link'] = array(
    'title' => t('Custom City Link'),
    'help' => t("Outputs a city field to a listing_location link."),
    'field' => array(
	  'field' => 'city',
      'handler' => 'listing_location_handler_field_city_link',
      'click sortable' => TRUE,
    ),
  );
  $data['location']['targeted_proximity'] = array(
    'title' => t('Targeted Proximity'),
    'help' => t("Proximity to the current user IP or cookie location."),
    'field' => array(
      'handler' => 'location_handler_field_location_distance',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'listing_location_handler_sort_location_distance',
    ),
    'filter' => array(
      'handler' => 'listing_location_handler_filter_proximity',
    ),
  );
   return $data;
}