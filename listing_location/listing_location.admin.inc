<?php
function listing_location_admin_settings_form() {
  // Do some pre form build configuration based on database values
  $countries = listing_location_get_countries();
  
  //  We check if this is a single country listing
  variable_set('listing_location_countries', count($countries));
  variable_set('listing_location_country_select', $countries);
  // We also check if this is a single state province listing.
  $result = db_query("SELECT DISTINCT(state) FROM {zipcodes}");
  $c = 0;
  while ($row = db_fetch_object($result)) {
    $state = $row->state;
    $c++;
  }
  variable_set('listing_location_states', $c);
  if ($c == 1) {
    $states = listing_location_get_states();
	
    variable_set('listing_location_default_state', $states[$state]);
	variable_set('listing_location_default_state_code', $state);
	drupal_set_message(t('Default province / state set to !state !code', array('!state' => $states[$state], '!code' => $state)));
  }
  else variable_del('listing_location_default_province');
  // This is a simple check to see that our views are in place
  if (_listing_location_check_views()) variable_set('listing_location_views', 1);
  else variable_set('listing_location_views', 0);
  // end of pre build configuration
  
  if (variable_get('listing_location_countries', 0)) {
    /*
	 *  DEFAULT ITEMS
	 */
    $form['defaults'] = array(
      '#type' => 'fieldset',
	  '#title' => t('Defaults'),
	  '#collapsible' => TRUE,
	  '#collapsed' => TRUE
    );
    $form['defaults']['listing_default_country'] = array(
      '#type'          => 'select',
	  '#options'		 => location_get_iso3166_list(),
      '#title'         => t('Default Country'),
	  '#description'   => t('This is the default country for listings.'),
      '#default_value' => variable_get('listing_default_country', 'nz'),
    );
    $form['defaults']['listing_default_location'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Default Location'),
	  '#description'   => t('In the absence of the geoip module, we need a default location to ensure the search has a starting point.  This should be in the form [city-name]: [state/province code] eg Hastings: HKB.  If this is incorrect the site will fail for new users. Note that this functionality applies only to those countries with the necessary data available in the location module zipcodes table.  If you are using a country which does not have that data then use this module '),
      '#default_value' => variable_get('listing_default_location', 'Hastings: HKB'),
    );
    $form['defaults']['listing_location_target_default_distance'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Default proximity'),
	  '#description'   => t('Enter a value for the default proximity in views proximity filters'),
      '#default_value' => variable_get('listing_location_target_default_distance', 25),
    );
    /*
	 *  SEARCH OPTIONS
	 */
    $form['options'] = array(
      '#type' => 'fieldset',
	  '#title' => t('Search Options'),
	  '#collapsible' => TRUE,
	  '#collapsed' => TRUE
    );
    $form['options']['listing_location_include_zipcode'] = array(
      '#type'          => 'checkbox',
      '#title'         => t('Include Zipcode Search'),
	  '#description'   => t('Some countries make greater use of zipcodes than others.  When we include zipcodes then the user has the option of entering a string or a zipcode number in the search field. Check the zipcode database first to ensure that your country has zipcodes.'),
      '#default_value' => variable_get('listing_location_include_zipcode', 0),
    );
    $form['options']['listing_location_distance_options'] = array(
      '#type'          => 'select',
	  '#options'       => listing_location_distance_options(),
	  '#multiple'      => TRUE,
      '#title'         => t('Distance Options'),
	  '#description'   => t('Select the distances you want to use in the distance search field.'),
      '#default_value' => variable_get('listing_location_distance_options', array()),
    );
    $form['options']['listing_location_include_map'] = array(
      '#type'          => 'checkbox',
      '#title'         => t('Include Map in Listings'),
	  '#description'   => t('Listings whose content type does not include Locations will not display a map.'),
      '#default_value' => variable_get('listing_location_include_map', 1),
    );
    $ctypes = variable_get('listing_content_types', array());
    if (!empty($ctypes)) {
      $form['options']['listing_location_ctypes'] = array(
        '#type'          => 'select',
	    '#options'		 => variable_get('listing_content_types', array()),
        '#title'         => t('Content Types'),
        '#default_value' => variable_get('listing_location_ctypes', array()),
        '#description'   => t('Listing content types to use for location search.  Note that these content types have to be <a href="/admin/listing/general">selected</a> for listings.'),
	    '#multiple'		 => TRUE
      );
    }
    else {
      $form['listing_location_ctypes'] = array(
        '#type'          => 'markup',
	    '#value'         => l(t('No content types are enabled for the listing module'), 'admin/listing/general'),
	  );
    }
    $form['options']['listing_location_exempt_params'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Exempted Parameters'),
	  '#description'   => t('You can make some parameters in your search url exempt from location search.  This refers to any parameter immediately following the listing url set here: admin/listing/general. Separate each parameter by a comma.'),
      '#default_value' => variable_get('listing_location_exempt_params', ''),
    );
    $form['options']['listing_location_select_search'] = array(
      '#type'          => 'checkbox',
      '#title'         => t('Use select search'),
	  '#description'   => t('The default search is a proximity search using the location database. Check this box to use a city select search.  This is only viable if you reduce the number of cities in your search scope to a number that can be handled by a select form.'),
      '#default_value' => variable_get('listing_location_select_search', 0),
    );
    $form['options']['listing_location_include_terms'] = array(
      '#type'          => 'checkbox',
      '#title'         => t('Include terms select'),
	  '#description'   => t('When this box is ticked then the term select form will appear on the search form.'),
      '#default_value' => variable_get('listing_location_include_terms', 1),
    );
  /*
  $categories = array();
  $vocabs = taxonomy_get_vocabularies();
  foreach ($vocabs as $vocab) {
    $categories[$vocab->vid] = $vocab->name;
  }
  $form['listing_search']['listing_select_search_vocab'] = array(
    '#type'          => 'select',
	'#options' 		 => $categories,
    '#title'         => t('Search Taxonomy'),
	'#description'   => t('The taxonomy to use in conducting location searches.'),
    '#default_value' => variable_get('listing_select_search_vocab', 0),
  );
  */
    /*
	 *  MAP ITEMS
	 */
    $form['map'] = array(
      '#type' => 'fieldset',
	  '#title' => t('Listing Map'),
	  '#collapsible' => TRUE,
	  '#collapsed' => TRUE
    );
    $form['map']['listing_location_map_width'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Map width'),
	  '#description'   => t('The width of the map on the web page in pixels.'),
      '#default_value' => variable_get('listing_location_map_width', 300),
    );
    $form['map']['listing_location_map_height'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Map height'),
	  '#description'   => t('The height of the map on the web page in pixels.'),
      '#default_value' => variable_get('listing_location_map_height', 200),
    );
    $form['map']['listing_location_map_zoom'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Map Zoom'),
	  '#description'   => t('The zoom of the map on the web page in pixels.'),
      '#default_value' => variable_get('listing_location_map_zoom', 6),
    );
    $form['map']['listing_location_map_zoom_threshold'] = array(
      '#type'          => 'select',
	  '#options'       => listing_location_distance_options(),
      '#title'         => t('Map Zoom Threshold'),
	  '#description'   => t('Select the distance value at which the zoom is reduced by a value of one, to ensure that the map is large enough. As an example, at a distance of 100 km, the map should be larger than a map where the distance is 25.'),
      '#default_value' => variable_get('listing_location_map_zoom_threshold', 100),
    );
    /*
	 *  VIEW ITEMS
	 */
    $form['views'] = array(
      '#type' => 'fieldset',
	  '#title' => t('Listing Views'),
	  '#collapsible' => TRUE,
	  '#collapsed' => TRUE
    );
    $form['views']['listing_location_view_per_ctype'] = array(
      '#type'          => 'checkbox',
      '#title'         => t('Custom View per Content Type'),
	  '#description'   => t('Check this box to configure a separate view for each listing content type.  You will need to create the relevant page view displays in either ListingsList or ListingsListbyCity depending on which search type you are using.  The default display or block displays will not be made available.  You will need to submit this form before you will be able to configure them.'),
      '#default_value' => variable_get('listing_location_view_per_ctype', 0),
    );
	if (variable_get('listing_location_view_per_ctype', 0)) {
	  if (variable_get('listing_location_select_search', 0)) $view_name = 'ListingsListbyCity';
	  else $view_name = 'ListingsList';
	  $view = views_get_view($view_name);
	  $options = array();
	  foreach ($view->display as $key => $display) {
		//if (substr($key,0,4) == "page") {
		   $options[$key] = $key;
		//}
	  }
	  foreach (variable_get('listing_location_ctypes', array()) as $ctype) {
        $form['views']['listing_location_view_display_'.$ctype] = array(
          '#type'          => 'select',
		  '#options'	   => $options,
          '#title'         => $ctype,
	      '#description'   => t('Select the view display to use for this content type'),
          '#default_value' => variable_get('listing_location_view_display_'.$ctype, ''),
        );
	  }
	}
    /*
	 *  FEATURED ITEMS
	 */
    $form['featured'] = array(
      '#type' => 'fieldset',
	  '#title' => t('Featured Listing Block'),
	  '#collapsible' => TRUE,
	  '#collapsed' => TRUE
    );
    $form['featured']['listing_location_featured_blocks'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Number of block displays'),
	  '#description'   => t('Enter the number of displays in the FeaturedListings view that you wish to process.  The only displays processed are block displays. Each display is processed one after the other.  If the required number of listings cannot be found in the first display then the second display is processed and so on.'),
      '#default_value' => variable_get('listing_location_featured_blocks', 2),
    );
	/*
    $form['defaults']['listing_location_featured_list_limit'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Number of items in targeted listing block'),
	  '#description'   => t('Yet to be implemented'),
      '#default_value' => variable_get('listing_location_featured_list_limit', 3),
    );
	*/
    $form['featured']['listing_location_featured_text'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Text'),
	  '#description'   => t('Text to appear on featured listing block. Use !city and !state for city and state/province.'),
      '#default_value' => variable_get('listing_location_featured_text', 'Service Providers near !city, !state'),
    );
	//  Display results of pre form build configuration
    $form['views_check'] = array(
      '#type' => 'markup',
	  '#value' => variable_get('listing_location_views', 0) ? '<div>'.t('Location Search Views are enabled').'</div>' : '<div style="color: red">'.t('Cannot find Location Search Views.  Please check instructions').'</div>',
    );

    switch(variable_get('listing_location_states', 0)) {
      case 0:
	    $value = t('There is no province data in the zipcodes table.  Please see instructions for inserting country zipcode data for the location module');
	    break;
      case 1:
	    $value = t('There is only one state / province in the zipcodes table so you may remove the state / province parameter from the url.');
	    break;
      default:
	    $value = t('Multiple state / provinces are available so state / province parameter should show.');
		if (!in_array('state', variable_get('listing_location_url_setting_array', array('country', 'state', 'city', 'type', 'term', 'distance')))) $value .= '<p>'. t('WARNING: You do not currently have the [state] token in your !settings. Please add this or you will get unexpected results.', array('!settings' => '/admin/listing/location_search/url')) .'</p>';
	    break;
    }
    $form['province_check'] = array(
      '#type' => 'markup',
	  '#value' => '<div>'.$value.'</div>',
    );
	/*
    if (variable_get('listing_location_states', 0)) {
      $form['listing_location_show_province_url'] = array(
        '#type'          => 'checkbox',
        '#title'         => t('Province / state in url.'),
        '#description'   => t('Untick this box if you do not want to show the province / state in the url. Default is on.'),
        '#default_value' => variable_get('listing_location_show_province_url', 1)
      );
  
    }
	*/
  }
  if (count($countries)) $message =  '<br />'. t('The following countries are enabled').': '. implode(", ", $countries);
  else $message = '';
  switch(variable_get('listing_location_countries', 0)) {
    case 0:
	  $value = t('There is no data in the zipcodes table.  Please see instructions for inserting country zipcode data for the location module');
	  break;
    case 1:
	  $value = t('There is only one country in the zipcodes table so you may remove the country parameter from the url.');
	  break;
    default:
	  $value = t('Multiple countries are available so country parameter should show');
	  if (!in_array('country', variable_get('listing_location_url_setting_array', array('country', 'state', 'city', 'type', 'term', 'distance')))) $value .= t('WARNING: You do not currently have the [country] token in your !settings. Please add this or you will get unexpected results.', array('!settings' => '/admin/listing/location_search/url'));
	  break;
  }
  $form['country_check'] = array(
    '#type' => 'markup',
	'#value' => '<div">'. $value . $message .'</div>',
  );
  /*
  if (variable_get('listing_location_countries', 0) == 1) {
    $form['listing_location_show_country_url'] = array(
      '#type'          => 'checkbox',
      '#title'         => t('Country in url.'),
      '#description'   => t('Untick this box if you do not want to show the country in the url. Default is on.'),
      '#default_value' => variable_get('listing_location_show_country_url', 1)
    );
  }
  */
  $form['#submit'][] = 'listing_location_admin_settings_submit';
  
  return system_settings_form($form);
}
function listing_location_admin_settings_validate($form, $form_state) {
  $default = explode(":",variable_get('listing_default_location', 'Hastings: HKB'));
  $count = db_result(db_query("SELECT COUNT(*) FROM {zipcodes} WHERE LOWER(city)='%s' AND state='%s'", strtolower($default[0]), strtoupper($state_codes[$default[1]])));
  if (!$count) form_set_error('listing_default_location', t('The default province / state / city combination is not in the zipcodes database.  Either change the default or import the data'));
  if (!is_numeric($form_state['values']['listing_location_featured_blocks'])) form_set_error('listing_location_featured_blocks', 'Number of block displays for Featured listing must be numeric.');
  
}
function listing_location_admin_settings_submit($form, $form_state) {
  variable_set('listing_location_ctype_number', count($form_state['values']['listing_location_ctypes']));
  cache_clear_all('*', 'cache');
}
/*
 *  URL Settings
 */
function listing_location_admin_url_setting_form() {
  $form['listing_location_url_setting'] = array(
      '#type'          => 'textfield',
      '#title'         => t('URL Setting'),
	  '#description'   => t('The url to use for location search.  This should be in the form of tokens as follows separated by \'/\': [country] = country; [state] = state/province; [state_code] = state/province code; [city] = city; [type] = content type; [term] = term; [distance] = distance.'),
      '#default_value' => variable_get('listing_location_url_setting', '[country]/[state]/[city]/[type]/[term]/[distance]'),
  );
  $form['listing_location_url_setting_zip'] = array(
      '#type'          => 'textfield',
      '#title'         => t('URL Setting for zip searches'),
	  '#description'   => t('The url to use for location search using zip codes.  This should be in the form of tokens as follows separated by \'/\': [country] = country; [zip] = the word \'zip\'; [zipcode] = zip code; [type] = content type; [term] = term; [distance] = distance.'),
      '#default_value' => variable_get('listing_location_url_setting_zip', '[country]/[zip]/[zipcode]/[type]/[term]/[distance]'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
	'#value' => t('Submit'),
  );
  return $form;
}
function listing_location_admin_url_setting_form_validate(&$form, &$form_state) {
  $tokens = array('[country]', '[state]', '[state_code]', '[city]', '[term]', '[type]', '[distance]');
  $ziptokens = array('[country]', '[zip]', '[zipcode]', '[term]', '[type]', '[distance]');
  $required = array('[city]', '[distance]');
  $ziprequired = array('[zip]', '[zipcode]', '[distance]');
  listing_location_clean_url($form_state['values']['listing_location_url_setting']);
  listing_location_clean_url($form_state['values']['listing_location_url_setting_zip']);
  // Process the state/city url
  $parts = explode("/", $form_state['values']['listing_location_url_setting']);
  foreach ($parts as $part) {
    if (!in_array($part, $tokens)) form_set_error('listing_location_url_setting', t('!part is not a valid token', array('!part' => $part)));
  }
  if (variable_get('listing_location_countries', 0) > 1 && !in_array('[country]', $parts)) form_set_error('listing_location_url_setting', t('You have more than one country enabled, therefore you should have country in the url'));
  if (variable_get('listing_location_states', 0) > 1 && !in_array('[state]', $parts)) form_set_error('listing_location_url_setting', t('You have more than one state/province enabled, therefore you should have state/province in the url'));
  $result = array_intersect($required, $parts);
  if (count($result) != count($required)) form_set_error('listing_location_url_setting', t('You need to have both [city] and [distance] in the state/city url'));
  // process the zip url
  $parts = explode("/", $form_state['values']['listing_location_url_setting_zip']);
  foreach ($parts as $part) {
    if (!in_array($part, $ziptokens)) form_set_error('listing_location_url_setting_zip', t('!part is not a valid token', array('!part' => $part)));
  }
  $result = array_intersect($ziprequired, $parts);
  if (count($result) != count($ziprequired)) form_set_error('listing_location_url_setting_zip', t('You need to have [zip], [zipcode] and [distance] in the zip search url'));  
}
function listing_location_admin_url_setting_form_submit($form, $form_state) {
  // Save this to use as default
  variable_set('listing_location_url_setting', $form_state['values']['listing_location_url_setting']);
  variable_set('listing_location_url_setting_zip', $form_state['values']['listing_location_url_setting_zip']);
  // Process the state/city url
  //  The use of tokens is handy to help the user understand the settings but we don't actually need them
  $url = str_replace(array('[', ']'), "", $form_state['values']['listing_location_url_setting']);
  $url = explode("/", $url);
  $array = array();
  foreach ($url as $value) $array[$value] = $value;
  variable_set('listing_location_url_setting_array', $array);
  //  We retain some of the old variables
  if (in_array('country', $url)) variable_set('listing_location_show_country_url', 1);
  else variable_set('listing_location_show_country_url', 0);
  if (in_array('state', $url)) variable_set('listing_location_show_province_url', 1);
  else variable_set('listing_location_show_province_url', 0);
  // Process the zip url
  $url = str_replace(array('[', ']'), "", $form_state['values']['listing_location_url_setting_zip']);
  $url = explode("/", $url);
  $array = array();
  foreach ($url as $value) $array[$value] = $value;
  variable_set('listing_location_url_setting_zip_array', $array);
  
  drupal_set_message(t('URL settings saved'));
}
function listing_location_clean_url(&$url) {
  $len = strlen($url);
  if (substr($url, 0, 1) == '/') {
    $url = substr($url, 1, $len);
	$len = $len - 1;
  }
  if (substr($url, ($len-1), $len) == '/') $url = substr($url, 0, ($len-1));
}
/**
 * Form builder to list and manage search form fields.
 *
 * @ingroup forms
 * @see listing_location_overview_search_fields_submit()
 * @see theme_listing_location_overview_search_fields()
 */
function listing_location_overview_search_fields() {
  $weights = variable_get('listing_location_field_weights', array());
  asort($weights);
  $fields = listing_location_proximity_search_form();
  if (empty($weights)) $weights = $fields;
  //drupal_set_message(print_r($fields,1));	
  $form = array('#tree' => TRUE);
  $form['explanation'] = array(
    '#type' => 'markup',
	'#value' => '<p>Set the order of fields in the search form.  Any disabled fields will show here whether you are using them or not</p>'
  );
  foreach ($weights as $field_id => $weight) {
    if ($field_id == "submit" || $field_id == "op" || substr($field_id, 0, 5) == "form_") {
	  unset($weights[$field_id]);
	  continue;
	}
    $form[$field_id]['#field'] = (array)$field;
    $form[$field_id]['name'] = array('#value' => check_plain($field_id));
    $form[$field_id]['weight'] = array('#type' => 'weight', '#delta' => 10, '#default_value' => $weights[$field_id]);
  }

  // Only make this form include a submit button and weight if more than one
  // field exists.
  if (count($fields) > 1) {
    $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  }
  return $form;
}

/**
 * Submit handler for fields overview. Updates changed field weights.
 *
 * @see listing_location_overview_search_fields()
 */
function listing_location_overview_search_fields_submit($form, &$form_state) {
  $weights = array();
  foreach ($form_state['values'] as $field_id => $field) {
    if ($field_id == "submit" || $field_id == "op" || substr($field_id, 0, 5) == "form_") {
	  continue;
	}
    //$form[$field_id]['#field']['weight'] = $form_state['values'][$field_id]['weight'];
    $weights[$field_id] = $form_state['values'][$field_id]['weight'];
  }
  variable_set('listing_location_field_weights', $weights);
}

/**
 * Theme the field overview as a sortable list of fields.
 *
 * @ingroup themeable
 * @see listing_location_overview_search_fields()
 */
function theme_listing_location_overview_search_fields($form) {
  $rows = array();
  foreach (element_children($form) as $key) {
    if (isset($form[$key]['name'])) {
      $field = &$form[$key];
      $row = array();
      $row[] = drupal_render($field['name']);
      if (isset($field['weight'])) {
        $field['weight']['#attributes']['class'] = 'field-weight';
        $row[] = drupal_render($field['weight']);
      }
      $rows[] = array('data' => $row, 'class' => 'draggable');
    }
  }
  if (empty($rows)) {
    $rows[] = array(array('data' => t('No fields available.'), 'colspan' => '5'));
  }

  $header = array(t('Name'));
  if (isset($form['submit'])) {
    $header[] = t('Weight');
    drupal_add_tabledrag('listing_location', 'order', 'sibling', 'field-weight');
  }
  return theme('table', $header, $rows, array('id' => 'listing_location')) . drupal_render($form);
}
/**
 * Form builder to list and manage search form fields.
 *
 * @ingroup forms
 * @see listing_location_overview_search_fields_submit()
 * @see theme_listing_location_overview_search_fields()
 */
function listing_location_overview_search_components() {
  $weights = variable_get('listing_location_field_components', array('form' => 1, 'map' => 2, 'listings' => 3));
  asort($weights);
  $form = array('#tree' => TRUE);
  $form['explanation'] = array(
    '#type' => 'markup',
	'#value' => '<p>Set the order of components in the search form.  The map will show here whether you are using it or not</p>'
  );
  foreach ($weights as $id => $weight) {
    if ($id == "submit" || $id == "op" || substr($id, 0, 5) == "form_") {
	  unset($weights[$id]);
	  continue;
	}
    $form[$id]['#field'] = (array)$field;
    $form[$id]['name'] = array('#value' => check_plain($id));
    $form[$id]['weight'] = array('#type' => 'weight', '#delta' => 10, '#default_value' => $weights[$id]);
  }

  // Only make this form include a submit button and weight if more than one
  // field exists.
  if (count($weights) > 1) {
    $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  }
  return $form;
}

/**
 * Submit handler for fields overview. Updates changed field weights.
 *
 * @see listing_location_overview_search_fields()
 */
function listing_location_overview_search_components_submit($form, &$form_state) {
  //drupal_set_message(print_r($form_state['values'],1));	
  $weights = array();
  foreach ($form_state['values'] as $id => $field) {
    if ($id == "submit" || $id == "op" || substr($id, 0, 5) == "form_") {
	  continue;
	}
    //$form[$id]['#field']['weight'] = $form_state['values'][$id]['weight'];
    $weights[$id] = $form_state['values'][$id]['weight'];
  }
  variable_set('listing_location_field_components', $weights);
}

/**
 * Theme the field overview as a sortable list of fields.
 *
 * @ingroup themeable
 * @see listing_location_overview_search_fields()
 */
function theme_listing_location_overview_search_components($form) {
  $rows = array();
  $weights = variable_get('listing_location_field_components', array());
  foreach (element_children($form) as $key) {
    if (isset($form[$key]['name'])) {
      $field = &$form[$key];
      $row = array();
      $row[] = drupal_render($field['name']);
      if (isset($field['weight'])) {
        $field['weight']['#attributes']['class'] = 'field-weight';
        $row[] = drupal_render($field['weight']);
      }
      $rows[] = array('data' => $row, 'class' => 'draggable');
    }
  }
  if (empty($rows)) {
    $rows[] = array(array('data' => t('No fields available.'), 'colspan' => '5'));
  }

  $header = array(t('Name'));
  if (isset($form['submit'])) {
    $header[] = t('Weight');
    drupal_add_tabledrag('listing_location', 'order', 'sibling', 'field-weight');
  }
  return theme('table', $header, $rows, array('id' => 'listing_location')) . drupal_render($form);
}

/*
 *  Filter forms
 */
function listing_location_admin_filter_form() {
  $output = '<p>'. t('Filterable content is determined by the contents of the zipcodes table which is created by the location module.  All records in this table are inserted by using the .sql file available in the location module directory database.  Currently only about a half a dozen countries are supported.  You may reduce the available records by filtering by province / state and by city.  It is advisable to filter by province first, otherwise the city select form will be unwieldly because it will include thousands of options.'). '</p>';
  if (variable_get('listing_location_countries', 1) > 1)  $output .= t('Use this form to select the country to filter.');
  $output .= listing_location_admin_selected_country();
  $form['explanation'] = array(
    '#type' => 'markup',
	'#value' => $output
  );
  if (variable_get('listing_location_countries', 1) > 1) {
    $countries = listing_location_get_countries();
    $form['country'] = array(
	  '#type' => 'select',
	  '#title' => t('Country'),
	  '#options' => $countries,
	  '#default_value' => isset($_SESSION['listing-admin']['country']) ? $_SESSION['listing-admin']['country'] : '',
	  '#description' => t('Select the country you wish to filter'),
	);
    $form['submit'] = array(
      '#type' => 'submit',
	  '#value' => t('Submit')
    );
  }
  return $form;
}
function listing_location_admin_filter_form_submit($form, $form_state) {
  $_SESSION['listing-admin']['country'] = $form_state['values']['country'];
}
function listing_location_admin_provinces_form() {
  //listing_enable();
  $provinces = array();
  if (variable_get('listing_location_countries', 1) > 1) {
    if (!isset($_SESSION['listing-admin']['country'])) {
	  drupal_set_message(t('Please select a country'));
	  drupal_goto('admin/listing/location_search/filter');
	}
    $result = db_query("SELECT DISTINCT(state) FROM zipcodes WHERE country='%s' ORDER BY state", $_SESSION['listing-admin']['country']);
	$country = $_SESSION['listing-admin']['country'];
  }
  else {
    $result = db_query("SELECT DISTINCT(state) FROM zipcodes ORDER BY state");
	$country = variable_get('listing_default_country', 'nz');
  }
  include(drupal_get_path('module','location').'/supported/location.'.strtolower($country).'.inc');
  $func = 'location_province_list_'.strtolower($country);
  $states = $func();
  while ($row = db_fetch_object($result)) {
    $provinces[$row->state] = $states[$row->state];
  }
  $output = '<p>'. t('Use this form if you want to restrict the states / provinces that are available on the search form autocomplete.'). '</p>';
  $output .= listing_location_admin_selected_country();

  $form['explanation'] = array(
    '#type' 		 => 'markup',
	'#value'		 => $output
  );
  $form['provinces'] = array(
    '#type'          => 'select',
	'#options'		 => $provinces,
    '#title'         => t('State / Provinces'),
	'#size'			 => 20,
	'#description'   => t('Select the states / provinces you wish to remove'),
    '#multiple'		 => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
	'#value' => t('Submit')
  );
  return $form;
}
function listing_location_admin_provinces_form_submit($form, $form_state) {
  if (variable_get('listing_location_countries', 1) > 1) {
    db_query("DELETE FROM {zipcodes} WHERE state IN ('". implode("','", $form_state['values']['provinces'])."') AND country='%s'", $_SESSION['listing-admin']['country']);
  }
  else {
    db_query("DELETE FROM {zipcodes} WHERE state IN ('". implode("','", $form_state['values']['provinces'])."')");
  }
}
function listing_location_admin_cities_form() {
  //listing_enable();
  $cities = array();
  if (variable_get('listing_location_countries', 1) > 1) {
    if (!isset($_SESSION['listing-admin']['country'])) {
	  drupal_set_message(t('Please select a country'));
	  drupal_goto('admin/listing/location_search/filter');
	}
    $result = db_query("SELECT DISTINCT(city) FROM zipcodes WHERE country='%s' ORDER BY city", $_SESSION['listing-admin']['country']);
  }
  else $result = db_query("SELECT DISTINCT(city) FROM zipcodes ORDER BY city");
  while ($row = db_fetch_object($result)) {
    $cities[$row->city] = $row->city;
  }
  $output = '<p>'. t('Use this form if you want to restrict the cities that are available on the search form autocomplete.  It is advisable to filter by province / state first.'). '</p>';
  $output .= listing_location_admin_selected_country();

  $form['explanation'] = array(
    '#type' 		 => 'markup',
	'#value'		 => $output
  );
  $form['cities'] = array(
    '#type'          => 'select',
	'#options'		 => $cities,
	'#size'			 => 20,
    '#title'         => t('Cities'),
	'#description'   => t('Select cities you wish to remove'),
    '#multiple'		 => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
	'#value' => t('Submit')
  );
  return $form;
}
function listing_location_admin_cities_form_submit($form, $form_state) {
  if (variable_get('listing_location_countries', 1) > 1) {
    db_query("DELETE FROM {zipcodes} WHERE city IN ('". implode("','", $form_state['values']['cities'])."') AND country='%s'", $_SESSION['listing-admin']['country']);
  }
  else {
    db_query("DELETE FROM {zipcodes} WHERE city IN ('". implode("','", $form_state['values']['cities'])."')");
  }
}
/*
 *  Setting to configure the location module country selection via hook_locationapi
 */
function listing_location_location_countries_form() {
  $form['listing_location_countries'] = array(
    '#title' => 'Countries',
	'#type' => 'select',
	'#multiple' => TRUE,
	'#options' => location_get_iso3166_list(),
	'#size' => 20,
	'#description' => 'Configure the country selection for node forms',
    '#default_value' => variable_get('listing_location_countries', location_get_iso3166_list())
  );
  return system_settings_form($form);
  
}
function listing_location_admin_selected_country() {
  if (variable_get('listing_location_countries', 1) > 1 && isset($_SESSION['listing-admin']['country'])) {
    $countries = location_get_iso3166_list();
    return '<p>'. t('The selected country is !country', array('!country' => $countries[$_SESSION['listing-admin']['country']])). '</p>';
  }
  else return;
}
function listing_location_get_countries() {
  $result = db_query("SELECT DISTINCT(country) FROM {zipcodes}");
  $countries = array();
  $list = location_get_iso3166_list();
  while ($row = db_fetch_object($result)) {
	$countries[$row->country] = $list[$row->country];
  }
  return $countries;
}
function listing_location_distance_options() {
  return array(1 => 1, 5 => 5, 10 => 10, 25 => 25, 50 => 50,  75 => 75, 100 => 100,  125 => 125,  150 => 150,  200 => 200, 250 => 250);
}

