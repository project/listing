/* 
 *   Listing-location.js jQuery functions  newzeal
 */
// set global variable
listing = {};

function listing_location_show_term_select(def) {
    show_list_throbber();
	
    var type = $('#edit-select-type').val();
	
	if(type == 'all') {
	  $('#edit-select-term').empty();
	  $('#edit-select-term-wrapper').attr('style', 'visibility: hidden');
	}
	else {
      $.post(Drupal.settings.basePath + '?q=listing/terms/js/' + type + '/' + def, { },
         function(contents) {
           if (contents != '') {
		     //alert(contents);
             $('#edit-select-term').empty().append(contents);
           }
		   if(type!='all') $('#edit-select-term-wrapper').attr('style', 'visibility: visible');
           hide_list_throbber();
         }
      );
	}
}
function getListings(block_id) {
  var search_value = $('#listing-location-targeted-listing-location-form .form-autocomplete').val();
  doSearch(search_value, block_id);

  return false;
}
function listing_location_country_change() {

  var country = $('#edit-country-wrapper #edit-country').val();
  show_list_throbber();
  $.post(Drupal.settings.basePath + '?q=listing/country/js/' + country, { },
         function(contents) {
		   $('#edit-select-target').val('');
		   
		   listing.country = contents;
           hide_list_throbber();
         }
  );
}
function doSearch(search_value,block_id) {
	show_list_throbber();
    $.ajax({
          type: "GET",
          url: Drupal.settings.basePath + '?q=featured_listings/' + search_value + '/' + block_id,
          dataType: "json",
          success: function(input){
          if (input.content != '') {
		     //$('#block-listing_location-'+block_id).show();
             $('#targeted-list').empty().append(input.content);
			 $('#block-listing_location-'+block_id+' h2').empty().append(input.heading);
           }
		   //else $('#block-listing_location-'+block_id).hide();
           hide_list_throbber();
         }
    });

}
function show_list_throbber() {
  $('#search-div-throbber').attr('style', 'background-image: url(' + Drupal.settings.basePath + 'misc/throbber.gif); background-repeat: no-repeat; background-position: 100% -20px;').html('&nbsp;&nbsp;&nbsp;&nbsp;');
}

function hide_list_throbber() {
  $('#search-div-throbber').removeAttr('style').empty();
}