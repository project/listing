<?php
/**
 *  Menu Callbacks
 */
function listing_address_settings_form() {
  // settings for default and content type
  $form['listing_address_ctype'] = array(
    '#type'          => 'select',
	'#options'		 => variable_get('listing_content_types', array()),
    '#title'         => t('Single Country'),
    '#default_value' => variable_get('listing_address_ctype', ''),
    '#description'   => t("Listing content type to use for address search.  Note that this content types has to be selected for listings. Only one currently available."),
  );
  if (variable_get('listing_address_ctype', '')) {
    $data = content_types(variable_get('listing_address_ctype', ''));
	$fields = array();
	foreach ($data as $key => $item) {
	 
	  if ($key == "fields") {
		foreach ($item as $array) {
	      if ($array['type'] == 'addresses_cck') $fields[$array['field_name']] = $array['field_name'];
		}
	  }
	}
    $form['listing_address_address_field'] = array(
      '#type'          => 'select',
	  '#options'	   => $fields,
      '#title'         => t('Addresses Field'),
      '#default_value' => variable_get('listing_address_address_field', ''),
      '#description'   => t("The addresses field to use with listing_address."),
    );
  }
  $form['listing_address_home_page'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Home Page URL'),
    '#default_value' => variable_get('listing_address_home_page', 'node'),
    '#description'   => t("Enter a valid URL for the page you wish to use as the home page for your addresses search."),
  );
  $form['listing_address_type_label'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Dynamic Block Label'),
    '#default_value' => variable_get('listing_address_type_label', 'Nodes by Type'),
    '#description'   => t("Enter text that you wish to appear in the dynamic nodes by type block."),
  );
  $form['listing_address_home_page_bc'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Home Page breadcrumb'),
    '#default_value' => variable_get('listing_address_home_page_bc', 'address search'),
    '#description'   => t("Enter the label you wish to apply to the breadcrumb that links to the addresses search home page."),
  );
  $views_list = array();
  foreach (views_get_all_views() as $view => $viewdata) {
    $views_list[$viewdata->name] = $viewdata->name;
  }
  $form['listing_address_country_view'] = array(
    '#type'          => 'select',
	'#options'		 => $views_list,
    '#title'         => t('View to use to display nodes by country'),
    '#default_value' => variable_get('listing_address_country_view', ''),
    '#description'   => t("A correct view for this purpose includes a page display and the relevant fields and url."),
  );
  $form['listing_address_province_view'] = array(
    '#type'          => 'select',
	'#options'		 => $views_list,
    '#title'         => t('View to use to display nodes by state / province'),
    '#default_value' => variable_get('listing_address_province_view', ''),
    '#description'   => t("A correct view for this purpose includes a page display and the relevant fields and url."),
  );
  if (module_exists('adv_taxonomy_menu')) {
    $result = db_query("SELECT tmid, name FROM {adv_taxonomy_menu}");
	$options = array();
	while ($row = db_fetch_object($result)) {
	  $options[$row->tmid] = $row->name;
	}
    $form['listing_address_adv_tax_menu_tmid'] = array(
      '#type'          => 'select',
	  '#options' 	   => $options,
      '#title'         => t('Advanced Taxonomy Menu'),
      '#default_value' => variable_get('listing_address_adv_tax_menu_tmid', 0),
      '#description'   => t("Advanced Taxonomy Menu is enabled.  Select a menu to use with listing_address."),
    );
  }
  $form['listing_address_single_country'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Single Country'),
    '#default_value' => variable_get('listing_address_single_country', 0),
    '#description'   => t("If you wish to use Address Search for a single country, tick this box and then select the country."),
  );
  $form['listing_address_country_select'] = array(
    '#type'          => 'select',
    '#title'         => t('Select Country'),
	'#options' 		 => listing_address_country_get(),
    '#default_value' => variable_get('listing_address_country_select', 'nz'),
    '#description'   => t("If you are using Address Search for a single country, select the country here.  This selection only applies if the Single Country box is ticked."),
  );
  $form['#validate'][] = 'listing_address_settings_validate';
  $form['#submit'][] = 'listing_address_settings_submit';
  return system_settings_form($form);
}
function listing_address_settings_validate($form, $form_state) {
//  We store the urls for the selected views
  $view = views_get_view(@$form_state['values']['listing_address_province_view']);
 
  if ($view->display['page_1']->display_options['path'] == '') form_set_error('listing_address_province_view', t('The selected state / province view does not appear to have a url'));
 
  $view = views_get_view(@$form_state['values']['listing_address_country_view']);
  if ($view->display['page_1']->display_options['path'] == '') form_set_error('listing_address_country_view', t('The selected country view does not appear to have a url'));

}
function listing_address_settings_submit($form, $form_state) {
  //  We store the urls for the selected views
  $view = views_get_view(variable_get('listing_address_province_view', ''));
  $url = str_replace("%", "", $view->display['page_1']->display_options['path']);
  $strlen = strlen($url);
  if (substr($url, ($strlen-1),1) == '/') $url = substr($url, 0, ($strlen-1));
  variable_set('listing_address_province_view_url', $url);
  drupal_set_message(t('province url').' '.$url);

  $view = views_get_view(variable_get('listing_address_country_view', ''));
  $url = str_replace("%", "", $view->display['page_1']->display_options['path']);
  $strlen = strlen($url);
  if (substr($url, ($strlen-1),1) == '/') $url = substr($url, 0, ($strlen-1));
  variable_set('listing_address_country_view_url', $url);
  drupal_set_message(t('country url').' '.$url);
  
  if (variable_get('listing_address_address_field', '')) {
    $type = content_types(variable_get('listing_address_ctype', ''));
	
    foreach ($type['tables'] as $table) {
	  drupal_set_message(print_r($table,1));
	  if (substr($table, 0, 12) == "content_type") variable_set('listing_address_address_table', $table);
    }
  }
}
