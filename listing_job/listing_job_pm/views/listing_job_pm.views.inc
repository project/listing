<?php

/**
 * Implementation of hook_views_data():
 * Present fields and filters for relationships between messages and jobs
 */
function listing_job_pm_views_data() {

  $data['listing_job_pm']['table']['group']  = t('Job Private Messages');
  $data['listing_job_pm']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
    'pm_message' => array(
      'left_field' => 'mid',
      'field' => 'mid',
    ),
  );
  $data['listing_job_pm']['nid'] = array(
    'title' => t('Job'),
    'help' => t('Job being applied for.'),
    'field' => array(
      'handler' => 'listing_jobappln_handler_field_job',
      'click sortable' => TRUE,
    ),
    // Information for accepting a nid as an argument
    'argument' => array(
      'handler' => 'listing_jobappln_handler_argument_job_nid',
      'parent' => 'views_handler_argument_numeric', // make sure parent is included
      'name field' => 'title', // the field to display in the summary.
      'numeric' => TRUE,
      'validate type' => 'nid',
    ),
    // Information for accepting a nid as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    // Information for sorting on a nid.
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['listing_job_pm']['view_messages'] = array(
    'title' => t('Message View'),
	'help' => t('The view page for messages for this job.'),
    'field' => array(
      'title' => t('Link'),
      'help' => t('Provide a link to the view page for messages for this job. Privatemsg_views module has to be installed for this to work'),
      'handler' => 'listing_job_pm_handler_field_msg_page',
    ),
  );

  $data['listing_job_pm']['mid'] = array(
    'title' => t('Message Thread'),
    'help' => t('The complete thread for this message.'),
	'operator' =>  array('=' => 'equals'),
    'field' => array(
      'handler' => 'listing_job_pm_handler_field_msg_link',
      'click sortable' => TRUE,
    ),
   );
	

  return $data;
}


function listing_job_pm_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'listing_job_pm') .'/views',
    ),
    'handlers' => array(
      // field handlers
      'listing_job_pm_handler_field_job' => array(
        'parent' => 'views_handler_field',
      ),
      'listing_job_pm_handler_argument_job_nid' => array(
        'parent' => 'views_handler_argument_numeric',
      ),
      'listing_job_pm_handler_field_msg_link' => array(
        'parent' => 'views_handler_field',
      ),
      'listing_job_pm_handler_field_msg_page' => array(
        'parent' => 'views_handler_field',
      ),
    )
  );
}

