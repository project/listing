<?php
/*
*   This module created as a sub module for listing module to provide job functionality
*   Country and province search using cck_address and country_select
*   Category search using adv_taxonomy_menu 
*/

/**
 * Implementation of hook_init()
 *
 */
function listing_job_init() {
  //if (variable_get('listing_job_single_country', 0)) $_SESSION['country'] = variable_get('listing_job_country_select', 'nz');
  if (module_exists('adv_taxonomy_menu')) {
    global $_node_settings;
    if (!isset($_node_settings) && $result = adv_taxonomy_menu_get_settings(variable_get('listing_job_adv_tax_menu_tmid', array()))) {
      $_node_settings = db_fetch_object($result);
      $_node_settings->vocab_order = unserialize($_node_settings->vocab_order);
    }
    $bc = adv_taxonomy_menu_breadcrumb();
    if (module_exists('listing_address')) $bc = array_merge(listing_address_breadcrumb(), $bc);
    if (!empty($bc)) drupal_set_breadcrumb($bc);
    if ($bc) drupal_set_title(strip_tags(implode(" ",$bc)));
  }
}
/**
 * Implementation of hook_help()
 */
function listing_job_help($path, $arg) {
  switch ($path) {
    case 'admin/help#listing_job':
      return '<p>Handles creation of job listing types and resumes.</p>';
  }
}

/**
 * Implementation of hook_menu()
 */
function listing_job_menu() {
  global $user;
  $items = array();
  $items['admin/listing/job'] = array(
    'title'              => 'Job Listings',
    //'description'        => 'Job settings.',
    'page callback'      => 'drupal_get_form',
    'page arguments'     => array('listing_job_settings_form'),
    'access arguments'   => array('administer site configuration'),
    'type'          => MENU_NORMAL_ITEM,
    'weight'            => 3,    
  );
  $items['admin/listing/job/general'] = array(
    'title'              => 'General',
    'access arguments'   => array('administer site configuration'),
    'type'          => MENU_DEFAULT_LOCAL_TASK,
    'weight'        => 0,
  );
  return $items;
}
/**
 * Implementation of hook_perm()
 */
function listing_job_perm() {
  return array(
  'apply for jobs',
  'manage job applications',
  'create job', 'edit own job', 'edit any job', 'delete own job', 'delete any job'
  );
}
/**
 * Implementation of hook_node_info().
 */
function listing_job_node_info() {
  return array(
    'job' => array(
      'name' => t('Job'),
      'module' => 'listing_job',
      'description' => t('A single job posting.'),
    )
  );
}

/**
 * Implementation of hook_form().
 */
function listing_job_form(&$node) {
  // The site admin can decide if this node type has a title and body, and how
  // the fields should be labeled. We need to load these settings so we can
  // build the node form correctly.
  $type = node_get_types('type', $node);

  if ($type->has_title) {
    $form['title'] = array(
      '#type' => 'textfield',
      '#title' => check_plain($type->title_label),
      '#required' => TRUE,
      '#default_value' => $node->title,
      '#weight' => -5
    );
  }

  if ($type->has_body) {
    // In Drupal 6, we can use node_body_field() to get the body and filter
    // elements. This replaces the old textarea + filter_form() method of
    // setting this up. It will also ensure the teaser splitter gets set up
    // properly.
    $form['body_field'] = node_body_field($node, $type->body_label, $type->min_word_count);
  }

  return $form;
}
/**
 * Implementation of hook_access()
 */
function listing_job_access($op, $node, $account) {
  if ($op == 'create') {
    return user_access('create job', $account);
  }

  if ($op == 'update') {
    if (user_access('edit any job', $account) || (user_access('edit own job', $account) && ($account->uid == $node->uid))) {
      return TRUE;
    }
  }

  if ($op == 'delete') {
    if (user_access('delete any job', $account) || (user_access('delete own job', $account) && ($account->uid == $node->uid))) {
      return TRUE;
    }
  }
}


/**
 * Implementation of hook_nodeapi().
 */
function listing_job_nodeapi(&$node, $op, $teaser = NULL, $page = NULL) {
  global $user;
    switch ($op) {
    case 'load':
      global $_node;
      $_node = $node;
    break;
    case 'view':
      if (in_array($node->type, variable_get('listing_job_ctypes', array()))) {
      if (module_exists('adv_taxonomy_menu')) {
        $result = adv_taxonomy_menu_node_breadcrumb($node->taxonomy);
        if (is_array($result['bc']) && module_exists('listing_address')) {
          $bc = array_merge(listing_address_breadcrumb($node->nid, $result['q']), $result['bc']);
        }
      } 
      elseif (module_exists('listing_address')) $bc = listing_address_breadcrumb($node->nid);
      drupal_set_breadcrumb($bc);
    }
     break;
    case 'insert':
    case 'update':
    case 'delete':
      // clear all relevant caches: cids with the term in the url
      if (isset($node->taxonomy)) {
        foreach ($node->taxonomy as $term) {
          //cache_clear_all('/'.$term->tid, 'job_country', TRUE);
          //cache_clear_all('/'.$term->tid, 'job_category', TRUE);
          //cache_clear_all('/'.$term->tid, 'job_body', TRUE);
        }
      }
      // Node is being deleted, delete it from the job table
      db_query("DELETE FROM {job} WHERE nid = %d", $node->nid);
      break;
  }
}



/**
 *  Menu Callbacks
 */
function listing_job_settings_form() {
  // settings for default and content type
  $form['listing_job_home_page'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Job Home Page'),
    '#default_value' => variable_get('listing_job_home_page', 'jobs'),
    '#description'   => t("Enter the Drupal url for the job home page that breadcrumb links go to."),
  );
  $form['listing_job_ctypes'] = array(
    '#type'          => 'select',
    '#title'         => t('Job Content Types'),
    '#options'       => variable_get('listing_content_types', array()),
    '#default_value' => variable_get('listing_job_ctypes', array()),
    '#description'   => t("Listing content types to use for jobs.  Note that these content types have to be selected for listings at admin/listing."),
    '#multiple'     => TRUE
  );
  
  $types = node_get_types();
  $options = array();
  foreach ($types as $key => $type) {
    if (!in_array($type->type, variable_get('listing_content_types', array()))) $options[$type->type] = $type->name;
  }
  $form['listing_job_resume_type'] = array(
    '#type'          => 'select',
    '#title'         => t('Resume Content Type'),
  '#options'     => $options,
    '#default_value' => variable_get('listing_job_resume_type', 'resume'),
    '#description'   => t("Content type to use for resumes."),
  );
  $form["listing_job_email_files"] = array(
  '#type' => 'checkbox',
  '#title' => t("Include file links in notification emails"),
  '#return_value' => 1,
  '#default_value' => variable_get("listing_job_email_files", 0),
  '#description' => t("Select this option if you want application email notifications to include links directly to resume files"),
  );
  return system_settings_form($form);
}

/*
 *  Original job functions
 */


function listing_job_apply() {
  global $user;
  $job_nid = (int)arg(2);
  $resume_nid = (int)arg(3);

  if (!$user->uid) {
    drupal_set_message(t('Please !login or !register to apply',
      array(
        '!login' => l(t('login'), 'user/login'),
        '!register' => l(t('register'), 'user/register')
      )));
    drupal_goto("node/$nid");
  }

  if (!user_access('apply for jobs')) {
    drupal_set_message(t('You are not authorized to apply for jobs.'));
    drupal_goto("node/$job_nid");
  }

  if (!$job_nid) {
    drupal_set_message(t('No job specified.'));
    drupal_goto("node/$job_nid");
  }

  if ($resume_nid) {
    listing_job_apply_do($job_nid, $resume_nid);
    drupal_goto("node/$job_nid");
  }

  $job = node_load(array('nid' => $job_nid));

  $resume_list = listing_job_resume_list($user->uid); 

  if (!$resume_list) {
  
    $msg = t('Please !create to apply', array('!create' => l(t('create a resume'), 'node/add/' . variable_get('listing_job_resume_type', 'resume'))));
    drupal_set_message($msg);
    drupal_goto("node/$job_nid");
  }
  
  if (count($resume_list) == 1) {
    drupal_goto("job/apply/$job_nid/$resume_nid{$resume_list[0]['nid']}");
  }

  $output .= '<br/>' . t('Position: ') . $job->title . '<br>';
  $output .= '<br/>' . t('Select from your resumes below, or !create',
    array('!create' => l(t('create a new resume'), "node/add")));
  $output .= '<br/>';

  foreach ($resume_list as $resume) {
    $view  = l(t('view'),  'node/' . $resume['nid']);
    $apply = l(t('apply'), "job/apply/$job_nid/$resume_nid" . $resume['nid']);
    $edit  = l(t('edit'),  'node/' . $resume['nid'] . '/edit');

    $rows[] = array(
      $resume['title'],
      format_interval(time() - $resume['changed']), 
      $view . ' ' . $apply . ' ' . $edit
      );
  }
  $headers = array(t('Resume Title'), t('Last Changed'), t('Operations'));

  $output .= theme('table', $headers, $rows);
  print theme('page', $output);  
}

function listing_job_resume_list($uid) {

  $result = db_query("SELECT n.nid, n.title, n.changed FROM {node} n
      WHERE n.type='%s' AND n.uid = %d" , variable_get('listing_job_resume_type', 'resume'), $uid);
  while ($row = db_fetch_array($result)) {
    $data[] = $row;
  }
  return $data;
}

function listing_job_apply_do($job_nid, $resume_nid) {
  global $user;

  if (listing_job_user_applied($user->uid, $job_nid)) {
    drupal_set_message(t('You already applied for this position.'));
    drupal_goto("node/$job_nid");
  }
  if (module_exists('jobappln')) {
    // Set up node
    jobappln_node_insert($user, $resume_nid, $job_nid);
  }

  db_query('INSERT INTO {job} (nid, uid, resume_nid, timestamp, status) VALUES (%d, %d, %d, %d, %d)',
    $job_nid, $user->uid, $resume_nid, time(), 1);

  listing_job_send_email($job_nid, $resume_nid);

  drupal_set_message(t('Thank you. Your application has been submitted.'));
  drupal_goto("node/$job_nid");
}

function listing_job_user_applied($uid, $job_nid) {
  if (db_result(db_query("SELECT COUNT(*) FROM {job} j
    INNER JOIN {node} r ON j.resume_nid = r.nid
    INNER JOIN {node} n ON n.nid = r.nid
    WHERE n.uid = %d
    AND j.nid = %d", $uid, $job_nid))) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}


/**
 * Implemetation of hook_theme().
 *
 */
function listing_job_theme() {
  return array(
    'listing_job_mail' => array(
      'file' => 'listing_job.module',
      'arguments' => array(
        'job_node' => NULL,
        'job_user' => NULL,
        'appln_node' => NULL,
        'resume_user' => NULL,
      ),
    ),
  );
} 

/**
 * Implementation of hook_mail().
 */
function listing_job_mail($key, &$message, $params) {
  $result = theme('listing_job_mail', $params['job_node'], $params['job_user'], $params['appln_node'], $params['resume_user']);
  $message['subject'] = $result['subject'];
  $message['body'] = $result['body'];
}

function listing_job_send_email($job, $appln) {
  global $user;

  $params['job_node']    = $job;
  $params['job_user']    = $job_user = user_load($job->uid);
  $params['appln_node'] = $appln;
  $params['resume_user'] = $resume_user = user_load($appln->jobappln_uid);

  $from = $resume_user->mail;
  $language = user_preferred_language($user);

  drupal_mail('listing_job', 'job_apply', $job_user->mail, $language, $params, $from);

  watchdog('listing_job', t("%name applied for job !number.", array('%name' => theme('placeholder', $resume_user->name . " <$from>"), '!number' => $job_node->nid)));
}

function theme_listing_job_mail($job_node, $job_user, $appln_node, $resume_user) {
  global $base_url;
  
  $site = variable_get('site_name', 'drupal site');
  $subject = t("[$site] [Job application] for [$job_node->title] by [$resume_user->name]");
  $body  = t("The following user has applied for the above job.\n");
  $body .= t("\nJob: @title",                          array('@title'  => $job_node->title));
  $body .= t("\nJob URL: @url",                        array('@url'    => $base_url . url("node/$job_node->nid")));
  $body .= t("\nApplicant name: @name",                array('@name'   => $resume_user->name));
  $body .= t("\nApplicant email: @email",              array('@email'  => $resume_user->mail));
  $body .= t("\nApplicant URL: @url",                  array('@url'    => $base_url . url("user/$resume_user->uid")));
  if (module_exists('listing_jobappln')) {
    $body .= t("\nApplication URL: @url",                array('@url'    => $base_url . url("node/$appln_node->nid")));
  }
  // send link to files, if attached
  if (variable_get("listing_job_email_files", 0)) {
    $file_links = array();
    
    // first, try to include files from any filefields
    if (module_exists('filefield') && $fields = filefield_get_field_list($resume_node->type)) {
      foreach ($fields as $field) {
        $files = $resume_node->$field['field_name'];
        foreach ($files as $file) {
          $file_links[] = $base_url . url($file['filepath']);
        }
      }
    }
    
    // next, check for file attachments from the core upload.module
    if (is_array($resume_node->files) && count($resume_node->files)) {   
    foreach ($resume_node->files as $file_obj) {
    // if this file is not "listed", skip it
    if (empty($file_obj->list)) {
      continue;
    }
    
    // NOTE: for an HTML email, uncomment the following line and comment out the latter instead
    //$file_links[] = l($file_obj->description, $base_url . url($file_obj->filepath));
    $file_links[] = $base_url . url($file_obj->filepath);
    }
    }
    
    if (count($file_links)) {
    $body .= t("\n\nResume file(s):\n"). implode($file_links, "/n");
    }
  }
  
  $body .= t("\n\nManage job applications at @manage", array('@manage' => $base_url . url("job/applications")));

  return(array(
    'subject' => $subject,
    'body'    => $body,
    ));
}


/**
 * Implementation of hook_views_api().
 */
function listing_job_views_api() {
  return array(
    'api' => 2,
    'path' => drupal_get_path('module', 'listing_job') .'/views',
  );
}