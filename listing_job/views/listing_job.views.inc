<?php
/**
 * Implementation of hook_views_handlers().
 */
function listing_job_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'listing_job') .'/views',
    ),
    'handlers' => array(
      'listing_job_handler_bycountry' => array(
        'parent' => 'views_handler_argument',
      ),
      'listing_job_handler_byprovince' => array(
        'parent' => 'views_handler_argument',
      ),
     
    ),
  );
}


/**
 * Implementation of hook_views_data().
 */
function listing_job_views_data() {
  $data['content_type_job']['table']['group']  = t('listing_job');

  $data['content_type_job']['table']['base'] = array(
    'field' => 'nid',
    'title' => t('Node'),
    'help' => t('Job listing.'),
  );

  // joins
  $data['content_type_job']['table']['join'] = array(
    //...to the node table
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  $data['content_type_job']['nid'] = array(
    'title' => t('Jobs by Country'),
    'help' => t('Select jobs by country.'),
	'operator' =>  array('=' => 'equals'),
    'argument' => array(
      'handler' => 'listing_job_handler_bycountry',
    ),
   );
  $data['content_type_job']['field_job_address_province'] = array(
    'title' => t('Jobs by Province'),
    'help' => t('Select jobs by province.'),
	'operator' =>  array('=' => 'equals'),
    'argument' => array(
      'handler' => 'listing_job_handler_byprovince',
    ),
   );
 
   return $data;
}