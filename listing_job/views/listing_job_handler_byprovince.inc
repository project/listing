<?php

class listing_job_handler_byprovince extends views_handler_argument {
  function query() {
    $parts = explode("-", arg(1));
    $table = $this->ensure_my_table();
    $table = $this->query->ensure_table('content_type_job');
    $this->query->add_where($this->options['group'], "$table.field_job_address_province = '%s' AND $table.field_job_address_country = '%s'", $parts[1], $parts[0]);
  }
}