<?php

class listing_job_handler_bycountry extends views_handler_argument {
  function query() {
    $table = $this->ensure_my_table();
    $table = $this->query->ensure_table('content_type_job');
    $this->query->add_where($this->options['group'], "$table.field_job_address_country  = '%s'", arg(1));
  }
}