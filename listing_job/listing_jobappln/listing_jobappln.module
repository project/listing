<?php

/**
 * Implementation of hook_help().
 */

function listing_jobappln_help($path, $arg) {
  switch ($path) {
    case 'admin/help#listing_jobappln':
      return t('Allows users to apply for jobs posted with a node being created for each job application.  This enables Views to be used to sort job applications on the basis of select fields and add comments to help in the process of employee selection.');
  }
}
/**
 * Implementation of hook_enable().
 *  Once installed we create the listing_jobappln fields we want to have
 */
function listing_jobappln_enable() {
   if (!function_exists('content_copy_import_form')) {
     drupal_set_message(t('Cannot find content_copy functions.  Please ensure that the content copy module is installed before installing job application module'));
	 drupal_goto('admin/build/modules');
   }
   $form_state = array();
   $form_state['values']['type_name'] = '<create>';
  
   $myFile = drupal_get_path('module', 'listing_jobappln') .'/includes/jobappln-cck-type.txt'; // your exported cck type file
   $fh = fopen($myFile, 'r');
   $theData = fread($fh, filesize($myFile));
   fclose($fh);
   
   $form_state['values']['macro'] = "$theData";
   if (!function_exists('content_copy_import_form')) require_once(drupal_get_path('module', 'content_copy').'/content_copy.module');
   drupal_execute('content_copy_import_form', $form_state);
   drupal_set_message('job application content type created');
}
/**
 * Implementation of hook_requirements().
 */
function listing_jobappln_requirements($phase) {
   $t = get_t();
   if ($phase == 'runtime') {
     $count = db_result(db_query("SELECT COUNT(*) FROM {node_type} WHERE type='jobappln'"));
	 if (!$count) {
       $requirements['listing_jobappln'] = array(
        'description' => $t('The job application content type is not present.  Enable the module content_copy and aopy the contents of listing_jobappln/includes/jobappln-cck-type.txt into your database using content type import admin/content/types/import', array('@url' => 'http://drupal.org/cron')), 
        'severity' => REQUIREMENT_ERROR, 
        'value' => $t('Content type missing'),
      );
	 
	 }
   }
}
/**
* Implementation of hook_menu().
*/
function listing_jobappln_menu() {
  $items = array();
  $items['admin/listing/job/listing_jobappln'] = array(
     'title'              => 'Job Applications',
     'page callback'      => 'drupal_get_form',
     'page arguments'     => array('listing_jobappln_settings_form'),
     'access arguments'   => array('administer site configuration'),
     'type' 			   => MENU_LOCAL_TASK,
 	 'weight' 		   => 1,
  );
  
  $items['jobappln/apply'] = array(
	'page callback' => 'listing_jobappln_apply',
	'page arguments' => array(2,3),
	'type' => MENU_CALLBACK,
	'access callback' => "_listing_jobappln_perm_apply_node",
	'access arguments' => array(2),
  );
  $items['jobappln/confirm'] = array(
	'page callback' => 'drupal_get_form',
	'page arguments' => array('listing_jobappln_confirm', 2,3),
	'type' => MENU_CALLBACK,
	'access callback' => "_listing_jobappln_perm_apply_node",
	'access arguments' => array(2),
  );
  return $items;
}
function _listing_jobappln_perm_apply_node($nid) {
  global $user;
  
  // find who posted the job
  $node = node_load($nid);
  
  // Can't apply for own job
  if (user_access('apply for jobs') && $node->uid != $user->uid && !_listing_jobappln_check($node->nid, $user->uid)) {
	return true;
  }
  
  return false;
}
function _listing_jobappln_check($nid, $uid) {
  return db_result(db_query("SELECT COUNT(*) FROM {listing_jobappln} WHERE job_nid = %d AND uid = %d" , $nid, $uid));
}

/**
 * Implementation of hook_perm().
 */
function listing_jobappln_perm() {
  return array('View all job applications');
}

/**
 * Implementation of hook_form_alter().
 */
function listing_jobappln_form_alter(&$form, &$form_state, $form_id) {
  global $user;
  //We need the status fields for views so lets prevent it from being removed
  unset($form['field_jobappln_status']['remove']);
  
  // Special handling for anonymous users
  if ($form_id == 'jobappln_node_form' && $user->uid == 0) {
    $title = db_result(db_query("SELECT title FROM {node} WHERE nid=%d", arg(3)));
	
	$form['title']['#default_value'] = t('Application for job').': '.$title;
    $form['jobappln_uid'] = array(
	  '#type' => 'hidden',
	  '#value' => $user->uid
	);
    $form['jobappln_job'] = array(
	  '#type' => 'hidden',
	  '#value' => arg(3)
	);
    $form['field_jobappln_status']['#access'] = FALSE;
	$form['field_jobappln_comments']['#access'] = FALSE;
    //unset($form['buttons']['submit']);
  }
  if ($form_id == 'resume_node_form' && arg(3) != '') {
    $form['#redirect'] = 'node/'.arg(3);
  }
}

/**
 * Implementation of hook_nodeapi().
 */
function listing_jobappln_nodeapi(&$node, $op, $teaser = NULL, $page = NULL) {
  global $user;
  switch ($op) {
    case 'load':
	  if ($node->type == 'jobappln') {
		$obj = db_fetch_object(db_query("SELECT * FROM {listing_jobappln} WHERE nid=%d", $node->nid));
		$node->job_nid = $obj->job_nid;
		$node->jobappln_uid = $obj->uid;
		$node->jobappln_name = db_result(db_query("SELECT name FROM {users} WHERE uid=%d", $obj->uid));
		$node->job_title = db_result(db_query("SELECT title FROM {node} WHERE nid=%d", $obj->job_nid));
	  }
	  break;
    case 'view':
	  // Any drupal_goto's or access denied's should be disabled for cron runs.
	  if ($_SERVER['REQUEST_URI'] == '/admin/reports/status/run-cron' || $_SERVER['SCRIPT_NAME'] == '/cron.php') $cron = TRUE;
	  else $cron = FALSE;
	  
	  if ($page && $node->type == "jobappln" && $node->preview != "Preview") {
	    // Only admin and the job author can view job applications
	    if (!user_access('View all job applications') && $user->uid != 1 && $user->uid != $node->uid && !$cron) {
		  drupal_goto(variable_get('listing_jobappln_redirect_page', 'jobs'));
	    }
		$node->content['body']['#value'] .= '<div>'.t('Applicant').': '.l($node->jobappln_name, 'user/'.$node->jobappln_uid).'</div>';
		$node->content['body']['#value'] .= '<div>'.t('Job').': '.l($node->job_title, 'node/'.$node->job_nid).'</div>';
 		$node->content['body']['#value'] .= drupal_get_form('listing_jobappln_status_form', $node);
	  }
	  // When we view a node we show applications made to selected users
	  if (in_array($node->type, variable_get('listing_job_ctypes', array())) && ($user->uid == $node->uid || $user->uid == 1 || user_access('View all job applications'))) {
	    $node->content['body']['#value'] .= views_embed_view('JobApplications', 'block_1', $node->nid);
	  }
	  // Resumes should only be viewable by the author  
	  if ($user->uid && $node->type == variable_get('listing_job_resume_type', 'resume') && $node->uid != $user->uid && !$cron) {
	    drupal_access_denied();
		drupal_goto('');
	  }
	  
	  break;
	case 'insert':
	  if ($node->type=="jobappln") {
	    
		//db_query("INSERT INTO {listing_jobappln} (nid, uid, job_nid) VALUES (%d, %d, %d)",$node->nid, $node->jobappln_uid, $node->jobappln_job);
		// When handling anonymous users we have to reassign ownership of the job application to the job creator
		if ($node->jobappln_uid == 0) {
		  $uid = db_result(db_query("SELECT uid FROM {node} WHERE nid=%d", $node->jobappln_job));
		  $node->uid = $uid;
		  drupal_set_message(t('Your application has been sent'));
		}
	  }
	  break;
    case 'delete':
	  // find all applications for this job and delete them all
	  if ($node->type == 'jobappln') {
	    db_query("DELETE FROM {listing_jobappln} WHERE nid=%d", $node->nid);
	    if ($node->jobappln_uid > 0) db_query("DELETE FROM {job} WHERE nid=%d AND uid=%d", $node->job_nid, $node->jobappln_uid);
	  }
	  if (in_array($node->type, variable_get('listing_job_ctypes', array()))) {
	    // When a job is deleted, we delete all job applications and remove it from the jobappn table
	    $result = db_query("SELECT nid FROM {listing_jobappln} WHERE job_nid=%d",$node->nid);
	    while ($row = db_fetch_object($result)) node_delete($row->nid);
		db_query("DELETE FROM {listing_jobappln} WHERE job_nid=%d", $node->nid);
	  }
      break;
  }
}
/**
 * Implementation of hook_link().
 */
function listing_jobappln_link($type, $node = NULL, $teaser = FALSE) {
  global $user;
  drupal_add_css(drupal_get_path('module', 'listing_jobappln').'/listing_jobappln.css');
  $links = array();
  // If not a job node, don't bother
  if ($type != 'node' || !in_array($node->type, variable_get('listing_job_ctypes', array()))) {
    return $links;
  }
  
  
  
  // Can't apply for own job
  if (user_access('apply for jobs') && $node->uid != $user->uid && !_listing_jobappln_check($node->nid, $user->uid)) {
	$links['jobappln_apply'] = array(
	  'title' => t('Apply for this job'),
	  'href' => "jobappln/apply/".$node->nid,
	);
	
  }
  else {
    // Now, determine why the user was denied
    
    if (!$user->uid) {
	  // User is not logged in
	  // NOTE: consider an admin-defined message here
	  $links['jobappln_apply'] = array(
	  'title' => t('Please !login or !register to apply',
		array(
		  '!login' => l(t('login'), 'user/login'),
		  '!register' => l(t('register'), 'user/register')
		)),
	  'html' => true,
	  );
    }
    else if (_listing_jobappln_check($node->nid, $user->uid)) {
	  // User has applied for this job
	  // NOTE: consider a link to withdraw application here, if permitted
	  //       - will need to check permission AND status of application
	  //       - if status = 0 , cannot withdraw
	  $links['jobapln_apply'] = array(
		'title' => t('You applied for this job'),
	  );
    }
    else {
	  // Catch-all for authenticated users without permissions
	  // NOTE: consider an admin-defined message here
	  // for now, show nothing;
    }
  }
  return $links;
}

function listing_jobappln_views_pre_view(&$view, &$display_id,	&$args) {
  global $user;
  
  // Access control for job applications.  We only want specific people to see this page: the author, user/1 or those given the role
  if ($view->name=="JobApplications") {
    $uid = db_result(db_query("SELECT uid FROM {node} WHERE nid=%d", arg(1)));
	if ($user->uid != $uid && !user_access('View all job applications')) {
	  // Access denied may cause problems where this view is used in othe places, so we simply unset it
	  unset ($view);
    }
  }
}

/**
 * Implementation of hook_views_api().
 */
function listing_jobappln_views_api() {
  return array(
    'api' => 2,
    'path' => drupal_get_path('module', 'listing_jobappln') .'/views',
  );
}
function listing_jobappln_status_form($form_state, $node) {
  $status = content_fields('field_jobappln_status');
  $values = explode("\n", $status['allowed_values']);
  foreach ($values as $value) $options[trim($value)] = trim($value);
  $form['appln_status'] = array(
    '#type'          => 'select',
    '#title'         => t('Status'),
	'#options' 	     => $options,
    '#default_value' => $node->field_jobappln_status[0]['value'],
    '#description'   => t("Change the status of this node."),
  );
  $form['appln_nid'] = array(
    '#type'          => 'hidden',
    '#value'         => $node->nid,
  );
  $form['submit'] = array(
    '#type'          => 'submit',
    '#value'         => t('Change'),
  );
  return $form;
}
function listing_jobappln_status_form_submit($form, $form_state) {
  $node = node_load($form_state['values']['appln_nid']);
  $node->field_jobappln_status[0]['value'] = $form_state['values']['appln_status'];
  node_save($node);
}
/*
 *   Menu callback 
 */
function listing_jobappln_apply($job_nid, $resume_nid=0) {
  global $user;
  if (!user_access('apply for jobs')) {
    drupal_set_message(t('You are not authorized to apply for jobs.'));
    drupal_goto("node/$job_nid");
  }

  if (!$job_nid) {
    drupal_set_message(t('No job specified.'));
    drupal_goto("node/$job_nid");
  }

  if ($resume_nid) {
    drupal_goto('jobappln/confirm/'.$job_nid .'/'. $resume_nid);
  }
  
  //  Anonymous users can apply for jobs by uploading a resume as an attachment to a listing_jobappln node
  if (!$user->uid && variable_get('listing_jobappln_anonymous_applns', 0)) {
    // Check if anonymous users have permissions
	$result = db_result(db_query("SELECT COUNT(*) FROM {permission} WHERE rid=1 AND perm LIKE '%create jobappln content%'"));
    if ($result) drupal_goto('node/add/jobappln/'.$job_nid);
	else {
	  drupal_set_message(t('Anonymous users have been given permission to apply for jobs but they haven\'t been given permission to create a job application'));
	  drupal_access_denied();
	  drupal_goto('');
	}
  }
  elseif (!$user->uid) {
    drupal_set_message(t('Please !login or !register to apply',
      array(
        '!login' => l(t('login'), 'user/login'),
        '!register' => l(t('register'), 'user/register')
      )));
    drupal_goto("node/$nid");
  }

  $job = node_load(array('nid' => $job_nid));

  $resume_list = listing_job_resume_list($user->uid); 

  if (!$resume_list) {
  
    $msg = t('Please !create to apply', array('!create' => l(t('create a resume'), 'node/add/resume/'.$job_nid)));
    drupal_set_message($msg);
    drupal_goto("node/$job_nid");
  }
  
  if (variable_get('listing_jobappln_immediate_apply', 1) && count($resume_list) == 1) {
    drupal_goto("jobappln/apply/$job_nid/$resume_nid{$resume_list[0]['nid']}");
  }

  $output .= '<br/>' . t('Position: ') . $job->title . '<br>';
  $output .= '<br/>' . t('Select from your resumes below, or !create',
    array('!create' => l(t('create a new resume'), "node/add")));
  $output .= '<br/>';

  foreach ($resume_list as $resume) {
    $view  = l(t('view'),  'node/' . $resume['nid'], array('query' => array('destination' => 'jobappln/apply/'.$job->nid)));
    $apply = l(t('apply'), "jobappln/apply/$job_nid/$resume_nid" . $resume['nid']);
    $edit  = l(t('edit'),  'node/' . $resume['nid'] . '/edit', array('query' => array('destination' => 'jobappln/apply/'.$job->nid)));

    $rows[] = array(
      $resume['title'],
      format_interval(time() - $resume['changed']), 
      $view . ' ' . $apply . ' ' . $edit
      );
  }
  $headers = array(t('Resume Title'), t('Last Changed'), t('Operations'));

  $output .= theme('table', $headers, $rows);
  print theme('page', $output);  
}
function listing_jobappln_confirm($form_state, $job_nid, $resume_nid) {
  if (user_access('apply for jobs') && $job_nid && $resume_nid) {
    $form['job_nid'] = array(
      '#type' => 'hidden',
	  '#value' => $job_nid,
    );
    $form['resume_nid'] = array(
	  '#type' => 'hidden',
	  '#value' => $resume_nid,
    );
    $job = node_load($job_nid);
	$resume = node_load($resume_nid);
    return confirm_form(
	  $form,
      t('Are you sure you want to apply for the job !job using this resume !resume ?', array('!job' => $job->title, '!resume' => $resume->title)),
      'node/'.$job_nid, 
	  t('You only have one opportunity to apply for this job.'),
      t('Apply'), 
	  t('Cancel')
   );

  }
  else return t('You do not have permissions to apply for jobs or the url is missing parameters.  Please try again');
}
function listing_jobappln_confirm_submit($form, $form_state) {
  global $user;
  $resume_nid = $form_state['values']['resume_nid'];
  $job_nid = $form_state['values']['job_nid'];
  // Check authenticity of resume - to avoid false url injection
  $resume = node_load($resume_nid);
  if ($user->uid != $resume->uid) {
    drupal_set_message(t('You do not have authority to use this resume'));
	drupal_goto('node/'.$job_nid);
  }
  if (listing_job_user_applied($user->uid, $job_nid)) {
    drupal_set_message(t('You already applied for this position.'));
    drupal_goto("node/".$job_nid);
  }


  $appln = listing_jobappln_node_insert($user, $resume_nid, $job_nid);
  db_query('INSERT INTO {listing_jobappln} (nid, uid, job_nid, resume_nid, timestamp) VALUES (%d, %d, %d, %d, %d)', $appln->nid, $job_nid, $user->uid, $resume_nid, time());

  $job = node_load($job_nid);
  listing_job_send_email($job, $appln);

  drupal_set_message(t('Thank you. Your application has been submitted.'));
  drupal_goto("node/$job_nid");
}
function listing_jobappln_settings_form() {
  // settings for default and content type
  $form['listing_jobappln_redirect_page'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Redirect for anonymous applicants'),
    '#default_value' => variable_get('listing_jobappln_redirect_page', 'jobs'),
    '#description'   => t("When an anonymous user has applied for a job we redirect them to a suitable page."),
  );
  $form['listing_jobappln_immediate_apply'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Immediate job apply.'),
    '#default_value' => variable_get('listing_jobappln_immediate_apply', 1),
    '#description'   => t("If the user only has one resume then apply immediately using that."),
  );
  $form['listing_jobappln_anonymous_applns'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Anonymous Applications'),
    '#default_value' => variable_get('listing_jobappln_anonymous_applns', 0),
    '#description'   => t("Tick this box to allow users to apply anonymously."),
  );
  return system_settings_form($form);
}
/**
 *  Called when creating a job application
 */
function listing_jobappln_node_insert($user, $resume_nid, $job_nid) {
	$job = node_load($job_nid);
	if ($resume_nid>0) {
      $resume = node_load($resume_nid);
	  unset($resume->title);  // This renders as a link and lets just allow the user to keep the name he calls the resume a personal thing
	  $node->body = node_view($resume);
	}
    $node->title = $job->title;
    $node->type = 'jobappln';
    //  We save a job application with the job creator's uid so that the job creator can edit it and so that the applicant no longer can
    $node->uid = $job->uid;
    $node->jobappln_uid = $user->uid;
    $node->jobappln_job = $job_nid;
    $node->field_jobappln_status[0]['value'] = 'pending';
	// We need to have some content in the brief field to get an overview of the application in a Views listing
    // Pass the node object values to modules for alteration.
    //module_invoke_all('listing_jobappln_insert', $node, $resume);

    node_save($node);
	return $node;
}