<?php
/**
 * Field handler to provide simple renderer that allows rendering the applicant name
 */
class listing_jobappln_handler_field_user_name extends listing_jobappln_handler_field_user {
  /**
   * Add uid in the query so we can test for anonymous if needed.
   */

  function render($values) {
    if ($values->{$this->aliases['uid']} > 0) {
      $name = db_result(db_query("SELECT name FROM {users} WHERE uid=%d", $values->{$this->aliases['uid']}));
	  return $name;
	}
    // Otherwise, there's no special handling, so return the data directly.
    else return $data;
  }
}