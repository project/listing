<?php
/**
 * Field handler to provide simple renderer that allows using a themed user link
 */
class listing_jobappln_handler_field_user_link extends listing_jobappln_handler_field_user {
  /**
   * Add uid in the query so we can test for anonymous if needed.
   */
  function init(&$view, &$data) {
    parent::init($view, $data);
    if (!empty($this->options['overwrite_anonymous'])) {
      $this->additional_fields['uid'] = 'uid';
    }
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['overwrite_anonymous'] = array('default' => FALSE);
    $options['anonymous_text'] = array('default' => '', 'translatable' => TRUE);

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['overwrite_anonymous'] = array(
      '#title' => t('Overwrite the value to display for anonymous users'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['overwrite_anonymous']),
      '#description' => t('If selected, you will see a field to enter the text to use for anonymous users.'),
    );
    $form['anonymous_text'] = array(
      '#title' => t('Text to display for anonymous users'),
      '#type' => 'textfield',
      '#default_value' => $this->options['anonymous_text'],
      '#process' => array('views_process_dependency'),
      '#dependency' => array(
        'edit-options-overwrite-anonymous' => array(1),
      ),
    );
  }

  function render_link($data, $values) {
    if (!empty($this->options['link_to_user']) || !empty($this->options['overwrite_anonymous'])) {
	  $this->options['alter']['alter_text'] = TRUE;
	  $this->options['alter']['text'] = db_result(db_query("SELECT name FROM {users} WHERE uid=%d", $values->{$this->field_alias}));
      if (!empty($this->options['overwrite_anonymous']) && !$account->uid) {
        // This is an anonymous user, and we're overriting the text.
        $this->options['alter']['text'] = check_plain($this->options['anonymous_text']);
      }
      elseif (!empty($this->options['link_to_user'])) {
        $this->options['alter']['make_link'] = TRUE;
        $this->options['alter']['path'] = "user/" . $values->{$this->field_alias};
      }
	  else {
	    $this->options['alter']['text'] = $values->{$this->field_alias};
	  }
    }
    // Otherwise, there's no special handling, so return the data directly.
    return $data;
  }
  function render($values) {
    return $this->render_link(check_plain($values->{$this->field_alias}), $values);
  }

}
