<?php

/**
 * Implementation of hook_views_data():
 * Present fields and filters for user data.
 */
function listing_jobappln_views_data() {
  $data['listing_jobappln']['table']['group']  = t('Job Application');
  $data['listing_jobappln']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );
  $data['listing_jobappln']['job_nid'] = array(
    'title' => t('Job'),
    'help' => t('Job being applied for.'),
    'field' => array(
      'handler' => 'listing_jobappln_handler_field_job',
      'click sortable' => TRUE,
    ),
    // Information for accepting a nid as an argument
    'argument' => array(
      'handler' => 'listing_jobappln_handler_argument_job_nid',
      'parent' => 'views_handler_argument_numeric', // make sure parent is included
      'name field' => 'title', // the field to display in the summary.
      'numeric' => TRUE,
      'validate type' => 'nid',
    ),
    // Information for accepting a nid as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    // Information for sorting on a nid.
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
 //  $data['node']['table']['group']  = t('Job Application');
   $data['listing_jobappln']['view_applications'] = array(
    'field' => array(
      'title' => t('Link'),
      'help' => t('Provide a link to applications made for this job.'),
      'handler' => 'listing_jobappln_handler_field_job_link',
    ),
  );

  $data['listing_jobappln']['applicant_uid'] = array(
   'title' => t('Job Applicant Link'),
    'help' => t('User who made the application.'),
	'operator' =>  array('=' => 'equals'),
    'field' => array(
	  'field' => 'uid',
      'handler' => 'listing_jobappln_handler_field_user_link',
      'click sortable' => TRUE,
    ),
    'filter' => array(
	  'field' => 'uid',
      'title' => t('Uid'),
      'handler' => 'views_handler_filter_user_current',
    ),
   );
  $data['listing_jobappln']['applicant_name'] = array(
   'title' => t('Job Applicant Name'),
    'help' => t('The user name only.'),
	'operator' =>  array('=' => 'equals'),
    'field' => array(
	  'field' => 'uid',
      'handler' => 'listing_jobappln_handler_field_user_name',
      'click sortable' => TRUE,
    ),
   );
	

  return $data;
}


function listing_jobappln_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'listing_jobappln') .'/views',
    ),
    'handlers' => array(
      // field handlers
      'listing_jobappln_views_handler_field_applicant' => array(
        'parent' => 'views_handler_field',
      ),
      'listing_jobappln_handler_field_user' => array(
        'parent' => 'views_handler_field',
      ),
      'listing_jobappln_handler_field_user_link' => array(
        'parent' => 'listing_jobappln_handler_field_user',
      ),
      'listing_jobappln_handler_field_user_name' => array(
        'parent' => 'listing_jobappln_handler_field_user',
      ),
      'listing_jobappln_handler_field_job' => array(
        'parent' => 'views_handler_field',
      ),
      'listing_jobappln_handler_argument_job_nid' => array(
        'parent' => 'views_handler_argument_numeric',
      ),
      'listing_jobappln_handler_field_job_link' => array(
        'parent' => 'views_handler_field',
      ),
    )
  );
}

