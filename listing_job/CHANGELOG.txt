CHANGELOG.txt
alpha2 - first release
Enabled dynamic job by type block
Enabled single country option

Sept 2 2011
Added delete and publish/unpublish tabs

Oct 2 2011
Moved delete, publish/unpublish to listing.module so it is available to all listings

Nov 5 2011
Added confirm for job application

Apr 16 2012
Fixed access denied problems: http://drupal.org/node/1350572
