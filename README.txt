listing project README.txt.  Note that all readme info for all modules is in this file

Un updated version of documentation can be found online: http://webdev.passingphase.co.nz/listing

The listing project includes a number of different options for listing and search.

***************
Overview

The listing project includes a number of different options for listing and search.


***************
listing.module
***************
This creates the ability to assign some content types as listing content types to be used in other modules and is not much use on its own.  Most people like to have some form of search and the listing project has two options, one using the location module and the other using the addresses module.

CONFIGURATION
Set the content types to be used for listings at admin/settings/listing

***********************
listing_location.module
***********************
Un updated version of documentation can be found online: http://webdev.passingphase.co.nz/listing-location

This enables search by listing using location proximity or location city
Filterable content is determined by the contents of the zipcodes table which is created by the location module.  All records in this table are inserted by using the .sql file available in the location module directory database.  Currently only about a half a dozen countries are supported.
Import location zipcodes for countries you are using
You can reduce the records in the table by filtering here: admin/setting/listings/filter
Location search can either be done using proximity or city which is set at 

CONFIGURATION
Required:
1.  Listing_location requires country data in the zipcodes table.  Locaton module comes with data for the following countries:
United States (us), Great Britain (uk), France (fr) , Germany (de), Switzerland (ch), Belgium (be), Australia (au), Norway (no), India (in).
2.  Go to admin/listing/location_search/general and set the default country to a country for which you have zipcode data
3.  On the same page set the default location in the form city: state code.  This is the value that first time visitors will receive and it needs to be valid or they may get unreliable results.
4.  Select the content types to be used for this module.  Only content types selected for listing at admin/settings/listing will be shown here
Optional:
5.  You can change the default proximity, include zipcode search, remove the map from listings and configure the size of the map
6.  You can use select search which creates a select box for cities rather than an autocomplete and is only recommended if you have a limited number of cities in your database
7.  You can use a different view for each content type.
8.  You can remove the country from the url.  Only use if using one country
9.  You can remove the state / province from the url.  Only use if using one state / province

Listing_location requires country data in the zipcodes table.  Locaton module comes with data for the following countries:
United States (us), Great Britain (uk), France (fr) , Germany (de), Switzerland (ch), Belgium (be), Australia (au), Norway (no), India (in).
Once you have data go to admin/listing/location_search/general and set the default location.  This is the  value that first time visitors will receive and it needs to be valid or they may get unreliable results.

Make sure you set the default country to a country for which you have zipcode data and select the content types to be used for this module

The following functionality is in the pipeline for future versions:
To add another country import the zipcodes_geolite.sql aupplied (mysql only) and install the listing process module which will enable you to add your country to the zipcode table from the geolite table.  You can add as many countries as you like.  

VIEWS
=======
listing_location comes with 5 views, two for the city select option, and three for the proximity option.  These are both run with filters for proximity, distance, category and content type inserted programmatically so only add filters that do not include these.  You can change the distance units in the view but make sure you change all the views that you are using.
The FeaturedListing view is set with the content type as story.  You will need to change this and other features to suit your use.  Featured listing works as follows:
- Listings are shown relative to the user's current location settings or the value entered into the form
- If you have the geoip module correctly installed, then first time visitors will be targeted according to their IP address
- Create as many block displays as you desire for processing content in consecutive order: block_1, block_2, block_3 etc
- Set how many items you wish to display in the block
- The script will process each view one after the other until the set number of items is found
- You can add any number of displays to the view with different filters on any filters that you like

GEOIP
If you install the geoip module: http://drupal.org/project/geoip then you can use the user IP as a fallback for first time users.  The current user location data will default to their location as determined by the maxmind database.  See install instructions for the geoip module.

**********************
listing_address.module
**********************
Un updated version of documentation can be found online: http://webdev.passingphase.co.nz/listing-address

This enables search by listing using the addresses module and the adv_taxonomy_menu module if desired.

listing_address is designed to be used with the following modules:
addresses - this supplies the country and province fields which enables search by province and country
adv_taxonomy_menu - this enables you to create a menu system to search on a heirarchy of vocabularies and enables search by category

The following may be required with the addresses module if state / province select field is not auto-filled.
country_select - this is a wrapper for addresses that enables javascript country, province/state selection
activeselect - the javascript functionality for the country, province/state selection


CONFIGURATION
At admin/listing/address_search set the following:
1.  listing content type to be used with address search.  Only one available at the moment.
2.  The addresses field to use in the search.  Note that currently this field must not be shared between content types or else it will not work.
3.  The url of the home page for listing_address (which might be a view) and the label you want to use for it in the breadcrumb
4.  Select two views, one for nodes by country and one for nodes by state / province.  Ensure that the page display view has a url that DOES NOT INCLUDE a '/' in it.  Currently that will break the module.  listing_job has an example of a view that is compatible with listing_address.  If you do not have a module that supplies a suitable view then enable listing_job and export the view into a new view of your choice.
5.  Single country: if you are using listing search with only one country you can do this and select the country.  This removes unnecessary items from the url.

Enable blocks as required

******************
listing_job.module
******************

Un updated version of documentation can be found online: http://webdev.passingphase.co.nz/listing-jobs

This enables listing of jobs and replaces the jobplus module.  Unlike the jobplus module the jobsearch module is no longer needed.

CONFIGURATION

***********************
listing_jobappln.module
***********************

Un updated version of documentation can be found online: http://webdev.passingphase.co.nz/listing-jobs

This enables users to apply for jobs listing using the listing_job.module



IF CHANGING FROM jobplus
You will need to reset hook_adv_taxonomy_menu_sql_alter in each menu system used, if you have used this hook at all. If not then no problem.

*******************
listing_menu.module
*******************
This modules supplies a number of menu solutions:
TAXONOMY MENU
If you are using taxonomy menu then you can select vocabularies to use as dynamic menus that are url sensitive and maintain the current selected location.  These menus are available as blocks

creates a menu system based on content types and taxonomy terms in a single level vocabulary.  This is currently only for use with listing_location.  The menu has the following structure:

Top level: content type name
Second level: term name

The main feature of this menu is that the links remain sensitive to the currently stored location.

Installation: On installation a custom menu is created and a block where it is displayed.  The block displays all terms which are used by listing content types as menu links in the hierarchy determined by the vocabulary structure of those terms.

Admin settings: you can now limit the content types you wish to show in this menu.

For each listing type create a vocabulary and add terms.  When you create listing types select a suitable term to apply to it.  The module will create a top level item for each vocabulary and then the second level items will be the terms in that vocabulary.  This is a simple implementation.

*******************
listing_role.module
*******************
This modules enables selected roles to have some cck fields displayed in their listings which are not displayed in listings belonging to other roles.  The intended use is to enable people to purchase listing privileges which gives their listings greater impact.

**********************
listing_premium.module
**********************

This module enables listings to be upgraded to higher levels when the person who created the listing pays a fee.

Requires:
uc_node_checkout - There is no need to set the content types for this because if listing nodes can be created for free and payment is only required on upgrading
uc_node_checkout_scheduler, which requires scheduler - these control the period of the upgrade subscription


Enable the module

A tab now appears on the node view with the label 'Premium Listings'.  Clicking on this will enable the node to be upgraded, or if already upgraded will provide the current details

Settings

permissions
- Set administer premium listings to a role that suits

listing_profile
- You can set a display on the node view page for the node owner with the link to the premium listing application if they have not already upgraded
- Add as many upgrade types as you require including name, description, fields and duration.  Note that these settings replace the uc_node_checkout settings at admin/store/settings/node-checkout

uc_node_checkout
- If you want node types to be published when paid for then use the uc_node_checkout settings at admin/store/settings/node-checkout otherwise leave unset

uc_node_checkout_scheduler
- Optinal: Set the node_checkout_scheduler settings for each node type at admin/content/node-type/[node-type]/node_checkout_scheduler.  If you set attributes for the node type, then you will need these for setting the start and end times for the node type.  If you don't then the listing_profile settings apply for duration.

listing_premium uses the scheduler cron function to trigger listing changes.

Developer
Listings has two hooks on the listing_view page which can be used to change the view
hook_listings_search_preview - change the view before listings module changes
hook_listings_search_view - change the view after listings module changes
Note that while listing_menu uses the same menu scripts as supplied by the menu module, the menu is not stored in the normal menu tables as a custom menu, instead it is stored in cache_menu_listing on a per user basis.